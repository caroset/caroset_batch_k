package com.example.batch.writer;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.batch.domain.NoticeDoList;
import com.example.batch.sendmail.AmazonSES;
import com.example.batch.sendmail.ApnsSendNotification;

public class SendMailWriter extends MyBatisBatchItemWriter<Map<String,Object>>{

	  private static final Log LOGGER = LogFactory.getLog(SendMailWriter.class);
	  @Autowired
	  private AmazonSES amazonSESField;

	  private SqlSessionTemplate sqlSessionTemplate;
	  @Autowired
	  private ApnsSendNotification apnsSendNotification;

	  private String statementId;
	  public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		    if (sqlSessionTemplate == null) {
		      this.sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory, ExecutorType.SIMPLE);
		    }
		  }

		  /**
		   * Public setter for the {@link SqlSessionTemplate}.
		   *
		   * @param sqlSessionTemplate a template object for use the {@link SqlSession} on the Spring managed transaction
		   */
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		    this.sqlSessionTemplate = sqlSessionTemplate;
		  }

		  /**
		   * Public setter for the statement id identifying the statement in the SqlMap
		   * configuration file.
		   *
		   * @param statementId the id for the statement
		   */
	public void setStatementId(String statementId) {
		    this.statementId = statementId;
		  }

	  @Override
	  public void afterPropertiesSet() {

	  }


	  /**
	   * {@inheritDoc}
	   */
	  @Override
	  public void write(final List<? extends Map<String,Object>> items) {


		//送信終わった結果
		String result="";
	    if (!items.isEmpty()) {

	      if (LOGGER.isDebugEnabled()) {
	        LOGGER.debug("Executing batch with " + items.size() + " items.");
	      }

	      for ( Map<String,Object> item : items) {
	    	  //メール送信のパラメータを設定する
	    	  if(item.get("mailAddress")!=null&& !"".equals(item.get("mailAddress"))) {
	    		  amazonSESField.setTo(String.valueOf(item.get("mailAddress")));
	    		  amazonSESField.setSubject(String.valueOf(item.get("title")));
	    		  amazonSESField.setHtmlbody(String.valueOf(item.get("context")));
	    		  amazonSESField.setTextbody(String.valueOf(item.get("context")));

	    		  //メール送信を実行する
	    		  try {
					result=amazonSESField.sendMail();
				} catch (IOException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}


	    	  }
	    	//プッシュ送信の場合アプリへメッセージをプッシュします
	    	  if(item.get("token")!=null &&! "".equals(item.get("token"))) {
	    		  apnsSendNotification.setAlert(String.valueOf(item.get("pushContext")));
	    		  apnsSendNotification.setDeviceToken(String.valueOf(item.get("token")));
	    	  }
	    	//お知らせ・やることリストを更新します
	    		NoticeDoList noticeDoList = (NoticeDoList) item.get("noticeDoList");
	    		sqlSessionTemplate.update(statementId, noticeDoList);
	    		//成功の場合
	    		if("sucess".equals(result)) {
	    			sqlSessionTemplate.update("com.example.batch.repository.NoticeQueueMapper.deleteByPrimaryKey", item.get("queueId"));
	    		}
	      }

	      }
	    }
}
