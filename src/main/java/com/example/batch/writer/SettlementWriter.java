package com.example.batch.writer;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;

import com.example.batch.util.CsvUtil;

public class SettlementWriter extends MyBatisBatchItemWriter<List<Object>>{

	  private static final Log LOGGER = LogFactory.getLog(SettlementWriter.class);



	  @Override
	  public void afterPropertiesSet() {

	  }


	  /**
	   * {@inheritDoc}
	   */
	  @Override
	  public void write(final List<? extends List<Object>> items) {

		  	Date date = new Date();
		  	//ファイル名前
		  	String fileName="";
			SimpleDateFormat dateFormatter = new SimpleDateFormat("YYYYMMdd");
			//csv出力
			//同じ日に何回連携するかの場合
			File fileExist = new File(".");
			String[] fileNameLists = fileExist.list();


			System.out.println(dateFormatter.format(date));
			StringBuffer sb = new StringBuffer();
			sb.append("prefix");
			sb.append("shopId");
			sb.append(dateFormatter.format(date));
			int count =1;
			for(String fileNameCheck: fileNameLists) {
				if(fileNameCheck.startsWith(sb.toString())) {
					count++;
				}
			}
			//連番を設定する
			if(count!=0) {
				sb.append("00"+count);
			}else {
				sb.append("001");
			}
			sb.append(".csv");
			fileName=sb.toString();


		//出力のデータを集まって
		List<List<Object>> dataList = new ArrayList<>();
	    if (!items.isEmpty()) {

	      if (LOGGER.isDebugEnabled()) {
	        LOGGER.debug("Executing batch with " + items.size() + " items.");
	      }
	      dataList.addAll(items);
	    }

		//出力ファイルのパスを設定します
		String path="";
		CsvUtil.createCSVFile(dataList, path,fileName);
//
//	      for ( Map<String,Object> item : items) {
//	    	  //メール送信のパラメータを設定する
//	    	  if(item.get("mailAddress")!=null&& "".equals(item.get("mailAddress"))) {
//	    		  amazonSESField.setTo(String.valueOf(item.get("mailAddress")));
//	    		  amazonSESField.setSubject(String.valueOf(item.get("title")));
//	    		  amazonSESField.setHtmlbody(String.valueOf(item.get("context")));
//	    		  amazonSESField.setTextbody(String.valueOf(item.get("context")));
//
//	    		  //メール送信を実行する
//	    		  try {
//					result=amazonSESField.sendMail();
//				} catch (IOException e) {
//					// TODO 自動生成された catch ブロック
//					e.printStackTrace();
//				}
//
//
//	    	  }
//	    	//プッシュ送信の場合アプリへメッセージをプッシュします
//	    	  if(item.get("token")!=null &&! "".equals(item.get("token"))) {
//	    		  apnsSendNotification.setAlert(String.valueOf(item.get("pushContext")));
//	    		  apnsSendNotification.setDeviceToken(String.valueOf(item.get("token")));
//	    	  }
//	    	//お知らせ・やることリストを更新します
//	    		NoticeDoList noticeDoList = (NoticeDoList) item.get("noticeDoList");
//	    		sqlSessionTemplate.update(statementId, noticeDoList);
//	    		//成功の場合
//	    		if("sucess".equals(result)) {
//	    			sqlSessionTemplate.update("com.example.batch.repository.NoticeQueueMapper.deleteByPrimaryKey", item.get("queueId"));
//	    		}
//	      }
//
//	      }
	    }
}
