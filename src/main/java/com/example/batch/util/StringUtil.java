package com.example.batch.util;

public class StringUtil {

	public static String leftAddZero(String str) {

		//null又は空の場合処理しない
		if(str==null || str.equals(""))
			return str;
		int length =str.length();
		//満たす位
		if(length==10) {
			return str;
		}
		//補足の長さ
		int lengthAdd = 10 -length;
		String zero="";
		for(int i=0;i<lengthAdd;i++) {
			zero=zero+"0";
		}

		return zero+str;
	}

}
