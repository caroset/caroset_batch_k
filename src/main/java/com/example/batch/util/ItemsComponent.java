
package com.example.batch.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author kang.lei
 * @ClassName: ItemsComponent
 * @Description: 共通項目の設定
 *
 */
public class ItemsComponent {

	public static void setItems(Items items) {

		Items itemsSet = items;

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date nowTime=df.parse(df.format(new Date()));
			//インサートユーゼーIDを設定する
			itemsSet.setInsertUser(Thread.currentThread() .getStackTrace()[1].getMethodName());
			//インサート時間を設定する
			itemsSet.setInsertTime(nowTime);
			//更新時間を設定する
			itemsSet.setUpdateTime(nowTime);
			//更新ユーゼーIDを設定する
			itemsSet.setUpdateUser(Thread.currentThread() .getStackTrace()[1].getMethodName());
			//バッジョンを設定する
			itemsSet.setVersion((short) 0);
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}




	}

}
