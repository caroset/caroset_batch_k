/**
 *
 */
package com.example.batch.util;

import java.util.Date;

/**
 * @author kang.lei
 * @ClassName: Items
 * @Description: 共通項目
 *
 */
public class Items {

	   private String insertUser;

	    private Date insertTime;

	    private String updateUser;

	    private Date updateTime;

	    private Short version;

	    public String getInsertUser() {
	        return insertUser;
	    }

	    public void setInsertUser(String insertUser) {
	        this.insertUser = insertUser == null ? null : insertUser.trim();
	    }

	    public Date getInsertTime() {
	        return insertTime;
	    }

	    public void setInsertTime(Date insertTime) {
	        this.insertTime = insertTime;
	    }

	    public String getUpdateUser() {
	        return updateUser;
	    }

	    public void setUpdateUser(String updateUser) {
	        this.updateUser = updateUser == null ? null : updateUser.trim();
	    }

	    public Date getUpdateTime() {
	        return updateTime;
	    }

	    public void setUpdateTime(Date updateTime) {
	        this.updateTime = updateTime;
	    }

	    public Short getVersion() {
	        return version;
	    }

	    public void setVersion(Short version) {
	        this.version = version;
	    }


}
