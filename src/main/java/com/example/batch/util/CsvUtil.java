package com.example.batch.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class CsvUtil {

	    /**
	     * CSV文件生成方法
	     * @param dataList
	     * @param outPutPath
	     * @param filename
	     * @return
	     */
	    public static File createCSVFile( List<List<Object>> dataList,
	            String outPutPath, String filename) {

	        File csvFile = null;
	        BufferedWriter csvWtriter = null;
	        try {
	            csvFile = new File(outPutPath + File.separator + filename + ".csv");
	            File parent = csvFile.getParentFile();
	            if (parent != null && !parent.exists()) {
	                parent.mkdirs();
	            }
	            csvFile.createNewFile();

	            // GB2312使正确读取分隔符","
	            csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
	                    csvFile), "GB2312"), 1024);
	            // 写入文件头部
	            //writeRow(head, csvWtriter);

	            // 写入文件内容
	            for (List<Object> row : dataList) {
	                writeRow(row, csvWtriter);
	            }
	            csvWtriter.flush();
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                csvWtriter.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	        return csvFile;
	    }

	    /**
	     * 写一行数据方法
	     * @param row
	     * @param csvWriter
	     * @throws IOException
	     */
	    private static void writeRow(List<Object> row, BufferedWriter csvWriter) throws IOException {
	        // 写入文件头部
	        for (Object data : row) {
	            StringBuffer sb = new StringBuffer();
	            String rowStr = sb.append("\"").append(data).append("\",").toString();
	            csvWriter.write(rowStr);
	        }
	        csvWriter.newLine();
	    }


	    /**
	     * CSV読み取り方
	     */
	    public static List<Object[]> readCSVFile(String fileName) {

	    	List<Object[]> listResult =new ArrayList<>();
	        try {
	            BufferedReader reader = new BufferedReader(new FileReader(fileName));//换成你的文件名
//	            reader.readLine();//第一行信息，为标题信息，不用,如果需要，注释掉
	            String line = null;
	            while((line=reader.readLine())!=null){
	                String item[] = line.split(",");//CSV格式文件为逗号分隔符文件，这里根据逗号切分
	                listResult.add(item);
	            }

	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return  listResult;
	    }

}
