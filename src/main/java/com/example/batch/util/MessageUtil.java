package com.example.batch.util;

public class MessageUtil {

	public static String messageParametersReplace(String context,String parameters,Integer startIndex) {
		//パラメータを分割にする
		String[] parameterList = parameters.split("\\t");
		//繰り返して 置換します
		for(int i = startIndex;i< parameterList.length;i++) {
			context=context.replace("＜パラメータ"+(i+1)+"＞", parameterList[i]);
		}
		return context;

	}

}
