package com.example.batch.config;

import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.batch.domain.MemberMonthlyAmountChargs;
import com.example.batch.processor.SettlementProcessor;
import com.example.batch.writer.SettlementWriter;

@Configuration
@EnableBatchProcessing
public class SettlementBatch {
	 private JobBuilderFactory jobBuilderFactory;

	    private StepBuilderFactory stepBuilderFactory;

	    private SqlSessionFactory sqlSessionFactory;

	    @Autowired
	    public SettlementBatch(JobBuilderFactory jobBuilderFactory,
	                       StepBuilderFactory stepBuilderFactory,
	                       SqlSessionFactory sqlSessionFactory) {
	        this.jobBuilderFactory = jobBuilderFactory;
	        this.stepBuilderFactory = stepBuilderFactory;
	        this.sqlSessionFactory = sqlSessionFactory;
	    }

	    @Bean
	    public MyBatisCursorItemReader<MemberMonthlyAmountChargs> mailSendTypeReader() {
	        final MyBatisCursorItemReader<MemberMonthlyAmountChargs> reader = new MyBatisCursorItemReader<>();
	        reader.setSqlSessionFactory(sqlSessionFactory);
	        reader.setQueryId("com.example.batch.repository.MemberMonthlyAmountChargsMapper.selectSettlement");
	        return reader;
	    }

	    @Bean
	    public SettlementProcessor processor() {
	        return new SettlementProcessor();
	    }

	    @Bean
	    public MyBatisBatchItemWriter<List<Object>> writer() {
	        final SettlementWriter writer = new SettlementWriter();
	        return writer;
	    }

	    @Bean
	    public Step readAccountAndWriteMember() {
	        return stepBuilderFactory
	                .get("readAccountAndWriteMember")
	                .<MemberMonthlyAmountChargs, List<Object>>chunk(1)
	                .reader(mailSendTypeReader())
	                .processor(processor())
	                .writer(writer())
	                .build();
	    }

	@Bean
	  public Job messageSendJob() {
	      return jobBuilderFactory
	              .get("messageSendJob")
	              .incrementer(new RunIdIncrementer())
	              .flow(readAccountAndWriteMember())
	              .end()
	              .build();
	  }


}
