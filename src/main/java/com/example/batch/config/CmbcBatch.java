package com.example.batch.config;

import java.util.Map;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.processor.CmbcProcessor;
import com.example.batch.reader.CmbcMyBatisCursorItemReader;
import com.example.batch.writer.SendMailWriter;

@Configuration
@EnableBatchProcessing
public class CmbcBatch {
	 private JobBuilderFactory jobBuilderFactory;

	    private StepBuilderFactory stepBuilderFactory;

	    private SqlSessionFactory sqlSessionFactory;

	    @Autowired
	    public CmbcBatch(JobBuilderFactory jobBuilderFactory,
	                       StepBuilderFactory stepBuilderFactory,
	                       SqlSessionFactory sqlSessionFactory) {
	        this.jobBuilderFactory = jobBuilderFactory;
	        this.stepBuilderFactory = stepBuilderFactory;
	        this.sqlSessionFactory = sqlSessionFactory;
	    }

	    @Bean
	    public MyBatisCursorItemReader<CarcassCofirmParameter> mailSendTypeReader() {
	        final CmbcMyBatisCursorItemReader reader = new CmbcMyBatisCursorItemReader();
	        reader.setSqlSessionFactory(sqlSessionFactory);
	        reader.setQueryId("com.example.batch.repository.MemberMapper.selectCmbc");
	        return reader;
	    }

	    @Bean
	    public CmbcProcessor processor() {
	        return new CmbcProcessor();
	    }

	    @Bean
	    public MyBatisBatchItemWriter<Map<String,Object>> writer() {
	        final SendMailWriter writer = new SendMailWriter();
	        writer.setSqlSessionFactory(sqlSessionFactory);
	        writer.setStatementId("com.example.batch.repository.NoticeDoListMapper.updateByMemberId");
	        return writer;
	    }

	    @Bean
	    public Step readAccountAndWriteMember() {
	        return stepBuilderFactory
	                .get("readAccountAndWriteMember")
	                .<CarcassCofirmParameter, Map<String,Object>>chunk(1)
	                .reader(mailSendTypeReader())
	                .processor(processor())
	                .writer(writer())
	                .build();
	    }

	@Bean
	  public Job messageSendJob() {
	      return jobBuilderFactory
	              .get("messageSendJob")
	              .incrementer(new RunIdIncrementer())
	              .flow(readAccountAndWriteMember())
	              .end()
	              .build();
	  }


}
