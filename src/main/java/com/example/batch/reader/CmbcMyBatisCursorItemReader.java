package com.example.batch.reader;

import java.util.Iterator;

import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.domain.Member;
import com.example.batch.service.CarcassMemberService;
import com.example.batch.service.MemberCouponService;

public class CmbcMyBatisCursorItemReader  extends MyBatisCursorItemReader<CarcassCofirmParameter>{

	  private Iterator<Member> cursorIterator;
	  @Autowired
	  private MemberCouponService memberCouponService;

	  @Autowired
	  private CarcassMemberService carcassMemberService;

	  @Autowired
	  private CarcassCofirmParameter carcassCofirmParameter;


	  public CmbcMyBatisCursorItemReader() {
	    setName(CmbcMyBatisCursorItemReader.class.getName());
	  }

	@Override
	  protected CarcassCofirmParameter doRead() throws Exception {
		Member next = null;
	    if (cursorIterator.hasNext()) {
	      next = cursorIterator.next();
	      Integer memberId = next.getMemberId();
	      //会員クーポンの設定をすると
	      memberCouponService.updateByMemberId(memberId);
	      memberCouponService.updateUseEndDay(memberId);
	      memberCouponService.updateByAreaPt(memberId);
	      //団体ドメインのドメイン終了していないのパラメータを検索する結果を取得する
	      carcassCofirmParameter=carcassMemberService.selectByMemberId(memberId);
	      carcassCofirmParameter.setMemberId(memberId);

	    }

	    return carcassCofirmParameter;
	  }
}
