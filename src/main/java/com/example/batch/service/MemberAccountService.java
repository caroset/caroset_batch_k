package com.example.batch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.repository.MemberAccountMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class MemberAccountService {

	@Autowired
	private MemberAccountMapper memberAccountMapper;

	public Integer selectCmbc(Integer memberId) {
		return memberAccountMapper.selectCmbc(memberId);
	}

}
