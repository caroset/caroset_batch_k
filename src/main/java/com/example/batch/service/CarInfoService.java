/**
 *
 */
package com.example.batch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.batch.domain.CarInfo;
import com.example.batch.repository.CarInfoMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
@Component
public class CarInfoService {

	@Autowired
	private CarInfoMapper carInfoMapper;
	/**
	 *
	 * @param autocarInsurance
	 * @Description 項目のインサート
	 * @return 検索件数
	 */
	public int insertSelective(CarInfo carInfo) {
		return carInfoMapper.insertSelective(carInfo);
	}
	/**
	 *
	 * @param carId
	 * @Description 車IDによると 自動車保険のデータを取得しました
	 * @return AutocarInsurance データ
	 */
	public List<Integer> selectMemberId(List<Integer> carId){
		return carInfoMapper.selectMemberId(carId);

	}

	/**
	 *
	 * @param record 自動車保険モデル
	 * @return 更新件数
	 *
	 */
	public int updateByPrimaryKeySelective(CarInfo carInfo) {

		return carInfoMapper.updateByPrimaryKeySelective(carInfo);
	}



//	  int deleteByPrimaryKey(Integer carId);
//
//	    int insert(AutocarInsurance record);
//
//	    int insertSelective(AutocarInsurance record);
//
//	    AutocarInsurance selectByPrimaryKey(Integer carId);
//
//	    int updateByPrimaryKeySelective(AutocarInsurance record);
//
//	    int updateByPrimaryKey(AutocarInsurance record);



}
