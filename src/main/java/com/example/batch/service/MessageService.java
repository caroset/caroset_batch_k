package com.example.batch.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.Message;
import com.example.batch.repository.MessageMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class MessageService {

	@Autowired
	private MessageMapper messageMapper;
	static List<Message> messageList;
	static Map<Integer,Message> messageMap= new HashMap<>();

	/**
	 * 検索するテーブルの全部の内容
	 * @return 通知キュー内容
	 */
	public  Map<Integer,Message> selectMessageCheck() {
		if(MessageService.messageList==null) {
			messageList = messageMapper.selectAll();
			for(Message message:messageList) {
				messageMap.put(message.getMessageId(), message);
			}
		}
		return messageMap;
	}




}
