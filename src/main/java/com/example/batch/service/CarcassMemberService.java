package com.example.batch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.domain.CarcassMember;
import com.example.batch.repository.CarcassMemberMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class CarcassMemberService {

	@Autowired
	private CarcassMemberMapper carcassMemberMapper;

	public List<CarcassMember> selectCmbc(Integer memberId) {
		return carcassMemberMapper.selectCmbc(memberId);
	}

	public int updateCmbc(Integer carcassMemberId) {
		return carcassMemberMapper.updateCmbc(carcassMemberId);
	}

	public int insertCmbc(CarcassMember carcassMember) {
		return carcassMemberMapper.insertCmbc(carcassMember);
	}

	public CarcassCofirmParameter selectByMemberId(Integer memberId) {
		return carcassMemberMapper.selectByMemberId(memberId);
	}

	public int selectCbmc(CarcassCofirmParameter carcassCofirmParameter) {
		return carcassMemberMapper.selectCbmc(carcassCofirmParameter);
	}
	public int insertCmbc(CarcassCofirmParameter carcassCofirmParameter) {
		return carcassMemberMapper.insertCmbc(carcassCofirmParameter);
	}

}
