/**
 *
 */
package com.example.batch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.batch.domain.AutocarInsurance;
import com.example.batch.repository.AutocarInsuranceMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
@Component
public class AutocarInsuranceService {

	@Autowired
	private AutocarInsuranceMapper autocarInsuranceMapper;
	/**
	 *
	 * @param autocarInsurance
	 * @Description 項目のインサート
	 * @return 検索件数
	 */
	public int insertSelective(AutocarInsurance autocarInsurance) {
		return autocarInsuranceMapper.insertSelective(autocarInsurance);
	}
	/**
	 *
	 * @param carId
	 * @Description 車IDによると 自動車保険のデータを取得しました
	 * @return AutocarInsurance データ
	 */
	public List<Integer> selectCarId(String times){
		return autocarInsuranceMapper.selectCarId(times);

	}

	/**
	 *
	 * @param record 自動車保険モデル
	 * @return 更新件数
	 *
	 */
	public int updateByPrimaryKeySelective(AutocarInsurance record) {

		return autocarInsuranceMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 *
	 * @param record 自動車保険モデル
	 * @return 更新件数
	 *
	 */
	public List<Integer> selectExpiredCarId(String times) {

		return autocarInsuranceMapper.selectExpiredCarId(times);
	}




}
