package com.example.batch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.NoticeSet;
import com.example.batch.repository.NoticeSetMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class NoticeSetService {

	@Autowired
	private NoticeSetMapper soticeSetMapper;
	/**
	 * 検索するテーブルの全部の内容
	 * @return 通知キュー内容
	 */
	public NoticeSet selectMessageCheck(Integer memberId) {
		return soticeSetMapper.selectMessageCheck(memberId);
	}




}
