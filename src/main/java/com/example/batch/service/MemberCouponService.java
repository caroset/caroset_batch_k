package com.example.batch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.repository.MemberCouponMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class MemberCouponService {

	@Autowired
	private MemberCouponMapper memberCouponMapper;


	public int selectByMemberId(Integer memberId) {
		return memberCouponMapper.selectByMemberId(memberId);
	}

	public int updateByExpired(Integer memberId) {
		return memberCouponMapper.updateByExpired(memberId);
	}

	public int insertCmbc(CarcassCofirmParameter carcassCofirmParameter) {
		return memberCouponMapper.insertCmbc(carcassCofirmParameter);
	}

	public List<Integer> selectThisMonthValid(Integer memberId) {
		return memberCouponMapper.selectThisMonthValid(memberId);
	}

	public Integer selectAreaPt(Integer memberId) {
		return memberCouponMapper.selectAreaPt(memberId);
	}

	public Integer updateByMemberId(Integer memberId) {
		return memberCouponMapper.updateByMemberId(memberId);
	}

	public Integer updateByAreaPt(Integer memberId) {
		return memberCouponMapper.updateByAreaPt(memberId);
	}

	public Integer updateUseEndDay(Integer memberId) {
		return memberCouponMapper.updateUseEndDay(memberId);
	}
}
