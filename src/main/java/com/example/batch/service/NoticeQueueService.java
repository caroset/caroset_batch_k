package com.example.batch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.NoticeQueue;
import com.example.batch.repository.NoticeQueueMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class NoticeQueueService {

	@Autowired
	private NoticeQueueMapper noticeQueueMapper;
	/**
	 * 検索するテーブルの全部の内容
	 * @return 通知キュー内容
	 */
	public NoticeQueue selectAll() {
		return noticeQueueMapper.selectAll();
	}




}
