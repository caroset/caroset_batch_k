package com.example.batch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.Share;
import com.example.batch.repository.ShareMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class ShareService {

	@Autowired
	private  ShareMapper shareMapper;

	/**
	 * @Description モデルによると アカウントの一件データを挿入しました
	 * @param Vic 車検証
	 */
	public void insert(Share share) {
		shareMapper.insert(share);
	}

	/**
	 * @Description 主キーによると アカウントの一件データを検索取得しました
	 * @param userId
	 */
	public Share selectByPrimaryKey(Integer shareId) {

		return shareMapper.selectByPrimaryKey(shareId);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param vic 車検証
	 */
	public int updateByPrimaryKeySelective(Share share) {

		return shareMapper.updateByPrimaryKeySelective(share);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param member 会員
	 */
	public List<Integer> selectBy30MinutePush(String times ) {

		return shareMapper.selectBy30MinutePush(times);
	}
	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param member 会員
	 */
	public List<Integer> selectMemberId(String stateKubun,String times ) {

		return shareMapper.selectMemberId(stateKubun,times);
	}
//
//	/**
//	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
//	 * @param member 会員
//	 */
//	public List<Integer> selectCarId(@Param("times") String times ) {
//
//		return vicMapper.selectCarId(times);
//	}


}
