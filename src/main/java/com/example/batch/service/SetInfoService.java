package com.example.batch.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.SetInfo;
import com.example.batch.repository.SetInfoMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class SetInfoService {

	@Autowired
	private  SetInfoMapper setInfoMapper;

	/**
	 * @Description モデルによると アカウントの一件データを挿入しました
	 * @param setInfo 決済情報
	 */
	public void insert(SetInfo setInfo) {
		setInfoMapper.insert(setInfo);
	}

	/**
	 * @Description 主キーによると アカウントの一件データを検索取得しました
	 * @param setInfo 決済情報
	 * @return 決済情報
	 */
	public SetInfo selectByPrimaryKey(SetInfo setInfo) {

		return setInfoMapper.selectByPrimaryKey(setInfo);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param setInfo 決済情報
	 * @return 更新件数
	 */
	public int updateByPrimaryKeySelective( SetInfo setInfo) {

		return setInfoMapper.updateByPrimaryKeySelective(setInfo);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param times システム時間
	 */
	public List<Integer> selectUserId(@Param("times") String times ) {

		return setInfoMapper.selectUserId(times);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param times システム時間
	 */
	public List<Integer> selectExpiredMemberId(@Param("times") String times ) {

		return setInfoMapper.selectExpiredMemberId(times);
	}


}
