package com.example.batch.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.Account;
import com.example.batch.repository.AccountMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class AccountService {

	@Autowired
	private AccountMapper accountMapper;

	/**
	 * @Description モデルによると アカウントの一件データを挿入しました
	 * @param member 会員
	 */
	public void insert(Account account) {
		accountMapper.insert(account);
	}

	/**
	 * @Description 主キーによると アカウントの一件データを検索取得しました
	 * @param member 会員
	 */
	public Account selectByPrimaryKey(Integer userId) {

		return accountMapper.selectByPrimaryKey(userId);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param member 会員
	 */
	public int updateByPrimaryKeySelective(Account account) {

		return accountMapper.updateByPrimaryKeySelective(account);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param member 会員
	 */
	public int updateMemberStatus(Map<String ,Object> map) {

		return accountMapper.updateMemberStatus(map);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param member 会員
	 */
	public int updateMemberStatusList(Map<String ,Object> map) {

		return accountMapper.updateMemberStatusList(map);
	}


}
