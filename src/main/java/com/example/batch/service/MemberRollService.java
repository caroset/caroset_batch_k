package com.example.batch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.MemberRoll;
import com.example.batch.repository.MemberRollMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class MemberRollService {

	@Autowired
	private MemberRollMapper memberRollMapper;

	public Integer updateByPrimaryKeySelective(MemberRoll memberRoll) {
		return memberRollMapper.updateByPrimaryKeySelective(memberRoll);
	}

}
