package com.example.batch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.Loan;
import com.example.batch.repository.LoanMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class LoanService {

	@Autowired
	private  LoanMapper loanMapper;

	/**
	 * @Description モデルによると アカウントの一件データを挿入しました
	 * @param Vic 車検証
	 */
	public void insert(Loan loan) {
		loanMapper.insert(loan);
	}

	/**
	 * @Description 主キーによると アカウントの一件データを検索取得しました
	 * @param userId
	 */
	public Loan selectByPrimaryKey(Loan loan) {

		return loanMapper.selectByPrimaryKey(loan);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param vic 車検証
	 */
	public int updateByPrimaryKeySelective(Loan loan) {

		return loanMapper.updateByPrimaryKeySelective(loan);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param member 会員
	 */
	public List<Integer> selectMemberId(String stateKubun,String times ) {

		return loanMapper.selectMemberId(stateKubun,times);
	}
//
//	/**
//	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
//	 * @param member 会員
//	 */
//	public List<Integer> selectCarId(@Param("times") String times ) {
//
//		return vicMapper.selectCarId(times);
//	}


}
