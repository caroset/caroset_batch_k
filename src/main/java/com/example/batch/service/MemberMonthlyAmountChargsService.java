package com.example.batch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.domain.MemberMonthlyAmountChargs;
import com.example.batch.repository.MemberMonthlyAmountChargsMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class MemberMonthlyAmountChargsService {

	@Autowired
	private MemberMonthlyAmountChargsMapper memberMonthlyAmountChargsMapper;

	public Integer insertCmbc(CarcassCofirmParameter carcassCofirmParameter) {
		return memberMonthlyAmountChargsMapper.insertCmbc(carcassCofirmParameter);
	}

	public int insertEnrollmentCoupon(CarcassCofirmParameter carcassCofirmParameter) {
		return memberMonthlyAmountChargsMapper.insertEnrollmentCoupon(carcassCofirmParameter);
	}

	public int insertAreaPt(CarcassCofirmParameter carcassCofirmParameter) {
		return memberMonthlyAmountChargsMapper.insertAreaPt(carcassCofirmParameter);
	}

	public int insertOther(CarcassCofirmParameter carcassCofirmParameter) {
		return memberMonthlyAmountChargsMapper.insertOther(carcassCofirmParameter);
	}

	public MemberMonthlyAmountChargs selectSettlement() {
		return memberMonthlyAmountChargsMapper.selectSettlement();
	}

	public int  updateMmacSettlementCondition(MemberMonthlyAmountChargs memberMonthlyAmountChargs) {
		return memberMonthlyAmountChargsMapper.updateMmacSettlementCondition(memberMonthlyAmountChargs);
	}

	public int  selectGmoMemberId(Integer memberId) {
		return memberMonthlyAmountChargsMapper.selectGmoMemberId(memberId);
	}

	public int  selectRSNP(Integer memberId) {
		return memberMonthlyAmountChargsMapper.selectRSNP(memberId);
	}

	public MemberMonthlyAmountChargs  selectByPrimaryKey(Integer memberId) {
		return memberMonthlyAmountChargsMapper.selectByPrimaryKey(memberId);
	}


}
