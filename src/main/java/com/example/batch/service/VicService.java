package com.example.batch.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.Vic;
import com.example.batch.repository.VicMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class VicService {

	@Autowired
	private  VicMapper vicMapper;

	/**
	 * @Description モデルによると アカウントの一件データを挿入しました
	 * @param Vic 車検証
	 */
	public void insert(Vic vic) {
		vicMapper.insert(vic);
	}

	/**
	 * @Description 主キーによると アカウントの一件データを検索取得しました
	 * @param userId
	 */
	public Vic selectByPrimaryKey(Integer userId) {

		return vicMapper.selectByPrimaryKey(userId);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param vic 車検証
	 */
	public int updateByPrimaryKeySelective(Vic vic) {

		return vicMapper.updateByPrimaryKeySelective(vic);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param member 会員
	 */
	public List<Vic> selectVicId(@Param("times") String times ) {

		return vicMapper.selectVicId(times);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param member 会員
	 */
	public List<Integer> selectCarId(@Param("times") String times ) {

		return vicMapper.selectCarId(times);
	}


}
