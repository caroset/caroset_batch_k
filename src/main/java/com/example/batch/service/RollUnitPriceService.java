package com.example.batch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.repository.RollUnitPriceMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class RollUnitPriceService {

	@Autowired
	private RollUnitPriceMapper RollUnitPriceMapper;

	public Integer selectRollUnitPrice() {
		return RollUnitPriceMapper.selectRollUnitPrice();
	}

}
