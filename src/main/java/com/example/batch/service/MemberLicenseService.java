package com.example.batch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.repository.MemberLicenseMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class MemberLicenseService {

	@Autowired
	private MemberLicenseMapper memberLicenseMapper;

	public Integer selectAreaCheck(Integer memberId) {
		return memberLicenseMapper.selectAreaCheck(memberId);
	}

}
