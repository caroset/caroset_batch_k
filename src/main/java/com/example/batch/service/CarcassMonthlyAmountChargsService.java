package com.example.batch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.repository.CarcassMonthlyAmountChargsMapper;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class CarcassMonthlyAmountChargsService {

	@Autowired
	private CarcassMonthlyAmountChargsMapper carcassMonthlyAmountChargsMapper;

	public Integer insertCmbc(CarcassCofirmParameter carcassCofirmParameter) {
		return carcassMonthlyAmountChargsMapper.insertCmbc(carcassCofirmParameter);
	}

}
