package com.example.batch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.SaveMessage;
import com.example.batch.repository.SaveMessageMapper;
import com.example.batch.util.ItemsComponent;



/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class SaveMessageService {

	@Autowired
	private  SaveMessageMapper saveMessageMapper;

	/**
	 * @Description モデルによると アカウントの一件データを挿入しました
	 * @param saveMessage 保存情報
	 */
	public void insert(SaveMessage saveMessage) {
		ItemsComponent.setItems(saveMessage);
		saveMessageMapper.insert(saveMessage);
	}
}
