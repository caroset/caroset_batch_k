package com.example.batch.tasklet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Member;
import com.example.batch.domain.Vic;
import com.example.batch.service.AccountService;
import com.example.batch.service.MemberService;
import com.example.batch.service.VicService;

@Component
public class VicTasklet implements Tasklet {

	@Autowired
	VicService vicService;
	@Autowired
	AccountService accountService;
	@Autowired
	MemberService memberService;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    	//システム時刻を取得する
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	String nowTimes=df.format(new Date());
    	//期限切れのユーザーIDを取得する
    	List<Vic> vicIdList= vicService.selectVicId(nowTimes);
    	List<Integer> userIdList =new ArrayList<>();
    	//会員情報
    	Member member =new Member();
    	Member memberResult;
    	//繰り返す
    	for(Vic vic: vicIdList) {
    		member.setLicenseNumber(vic.getVicId());
    		//アカウントの会員ステータスを更新する
    		memberResult = memberService.selectByVicId(member);
    		if(memberResult == null) {
    			break;
    		}
    		//userIdList.add(memberResult.getUserId());
    	}
    	//ユーザーテーブル更新パラメータ
    	Map<String,Object > mapParameter = new HashMap<>();
    	mapParameter.put("memberStatus", "5");
    	mapParameter.put("userId", userIdList);
    	accountService.updateMemberStatus(mapParameter);

    	//TODO メセッジ送信
    	System.out.println(userIdList.get(0));
    	  return RepeatStatus.FINISHED;
    }
}