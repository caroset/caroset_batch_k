package com.example.batch.tasklet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.SaveMessage;
import com.example.batch.service.SaveMessageService;
import com.example.batch.service.ShareService;

@Component
public class ReviewShareNoticeTasklet implements Tasklet {

	@Autowired
	ShareService shareService;
	@Autowired
	SaveMessageService saveMessageService;



    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    	//システム時刻を取得する
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	String nowTimes=df.format(new Date());

    	SaveMessage saveMessage =new SaveMessage();
    	//マラメータ
    	//七日を超える未評価のシェアIDを取得する
    	List<Integer> memberIdList = shareService.selectBy30MinutePush(nowTimes);
    	//会員番号を取得する
    	if(memberIdList!=null) {
    			//メールアドレースを取得する
    			//送信する
    			int count=100001;
    			for(Integer memberId: memberIdList) {
    				//メールアドレースを取得する
        			//TODO 送信する
    	        	saveMessage.setSaveMessageId(count++);
    	        	saveMessage.setMemberId(memberId);
    	        	saveMessage.setContent("七日以上されている、評価をして　ください");
    	        	saveMessageService.insert(saveMessage);




    			}
    		}
    		return RepeatStatus.FINISHED;
    }
}