package com.example.batch.tasklet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Member;
import com.example.batch.service.MemberService;

@Component
public class LicenseExpiredTasklet implements Tasklet {

	@Autowired
	MemberService memberService;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    	//システム時刻を取得する
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String nowTimes=df.format(new Date());
    	String expiredStatus= "01";

    	//会員番号を取得する
    	List<Integer> userIdList = memberService.selectByExpiredStatus(expiredStatus,nowTimes);
    	Member member;
    	//会員番号を繰り返す
    	for(Integer userId : userIdList) {
    		//TODO メール送信
    		//メールアドレースを取得する
    		//成功の場合
    		member=new Member();
    		//member.setExpiredStatus(expiredStatus);
    		//member.setUserId(userId);
    		memberService.updateExpiredStatus(member);
    	}
    	  return RepeatStatus.FINISHED;
    }
}