package com.example.batch.tasklet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.MemberMonthlyAmountChargs;
import com.example.batch.service.MemberMonthlyAmountChargsService;
import com.example.batch.util.CsvUtil;
import com.example.batch.util.DateUtils;

@Component
public class SettlementResultTasklet implements Tasklet {
	@Autowired
	private MemberMonthlyAmountChargsService memberMonthlyAmountChargsService;
	@Autowired
	private MemberMonthlyAmountChargs memberMonthlyAmountChargs;
	private Date date = new Date();

	private String[] codeArr= {"G02","G023","G04","G05","G06","G07","G08","G09","G10",
			"G11","G12","G13","G14","G15","G16","G17","G18","G19","G20",
			"G21","G22","G23","G24","G25","G26","G27","G28","G29","G30",
			"G31","G32","G33","G34","G35","G36","G37","G38","G39","G40",
			"G41","G42","G43","G44","G45","G46","G47","G48","G49","G50",
			"G51","G52","G53","G54","G55","G56","G57","G58","G59","G60",
			"G61","G62","G63","G64",
			"G83","G84","G85","G86","G87","G88","G89","G90",
			"G91","G92","G93","G94","G95","G96","G97","G98","G99"};
	private List<String> errCode=new ArrayList<String>(Arrays.asList(codeArr));
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    	//ファイル名前
    	String fileName ="";
    	Integer memberMonthlyAmountChargsId;
    	String stateCode;
    	Date processDate;
    	boolean controlFlg =false;
    	List<Object[]> csvContextList =CsvUtil.readCSVFile(fileName);
    	MemberMonthlyAmountChargs memberMonthlyAmountChargsCheck = new  MemberMonthlyAmountChargs();
    	if(csvContextList!=null && csvContextList.size() !=0) {

    		//繰り返し
    		for(Object[] context: csvContextList) {
    			memberMonthlyAmountChargsId = (Integer)context[13];
    			stateCode = (String)context[24];
    			processDate = (Date)context[23];
    			memberMonthlyAmountChargsCheck = memberMonthlyAmountChargsService.selectByPrimaryKey(memberMonthlyAmountChargsId);
    			memberMonthlyAmountChargs.setMemberMonthlyAmountChargsId(memberMonthlyAmountChargsId);
    			//状態コード"COMPLETE"
    			if(stateCode.equals("COMPLETE")) {
    				controlFlg=true;
    				memberMonthlyAmountChargs.setMmacSettlementCondition("決済OK");
    				memberMonthlyAmountChargs.setMmacSettlementDate(processDate);
    			//状態コード"INVALID"
    			}else if(stateCode.equals("INVALID")) {
    				controlFlg=true;
    				memberMonthlyAmountChargs.setMmacSettlementCondition("決済エラー");
    				memberMonthlyAmountChargs.setMmacSettlementDate(processDate);
    			//状態コード"FAIL"
    			}else if(stateCode.equals("FAIL")) {
    				String errCodeCheck=(String)context[22];
    				if(errCode.contains(errCodeCheck)) {
	    				boolean checkFlg= DateUtils.sameDate(date, memberMonthlyAmountChargsCheck.getMmacMonth());
	    				if("決済中".equals(memberMonthlyAmountChargsCheck.getMmacSettlementCondition())&&checkFlg) {
	    					controlFlg=true;
	    					memberMonthlyAmountChargs.setMmacSettlementCondition("決済NG");
	        				memberMonthlyAmountChargs.setMmacSettlementDate(processDate);
	    				}
	    				if("再決済中".equals(memberMonthlyAmountChargsCheck.getMmacSettlementCondition())) {
	    					//当月一日
	    					if(checkFlg) {
	    						controlFlg=true;
	    					memberMonthlyAmountChargs.setMmacSettlementCondition("決済NG");
	        				memberMonthlyAmountChargs.setMmacSettlementDate(processDate);
	    					}
	    					//先月一日
	    					Calendar cal = Calendar.getInstance();
	    					cal.add(Calendar.MONTH, -1);
	    					cal.set(Calendar.DATE, 1);
	    					if(DateUtils.sameDate(memberMonthlyAmountChargsCheck.getMmacMonth(),cal.getTime())) {
	    						controlFlg=true;
	    						memberMonthlyAmountChargs.setMmacSettlementCondition("二ヶ月NG");
	            				memberMonthlyAmountChargs.setMmacSettlementDate(processDate);
	            				//TODO 2ヶ月NGの場合は会員に通知も必要
	    					}
	    					if(cal.getTime().getTime() > memberMonthlyAmountChargsCheck.getMmacMonth().getTime()) {
	    						controlFlg=true;
	    						memberMonthlyAmountChargs.setMmacSettlementCondition("決済停止");
	            				memberMonthlyAmountChargs.setMmacSettlementDate(processDate);
	            				//TODO ※この場合は、強制退会処理も必要
	            				//TODO ※強制退会の場合は会員に通知も必要
	    					}
	    				}
	    			//エラーコード：上記以外のエラーコード
    				}else {
    					controlFlg=true;
    					memberMonthlyAmountChargs.setMmacSettlementCondition("決済エラー");
        				memberMonthlyAmountChargs.setMmacSettlementDate(processDate);
    				}
    			}
    			if(controlFlg) {
    				memberMonthlyAmountChargsService.updateMmacSettlementCondition(memberMonthlyAmountChargsCheck);
    			}

    		}
    	}else{
    		return RepeatStatus.FINISHED;
    	}


    	//TODO メセッジ送信
    	  return RepeatStatus.FINISHED;
    }
}