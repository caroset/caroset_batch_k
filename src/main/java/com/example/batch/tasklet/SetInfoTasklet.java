package com.example.batch.tasklet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.service.AccountService;
import com.example.batch.service.SetInfoService;

@Component
public class SetInfoTasklet implements Tasklet {

	@Autowired
	SetInfoService setInfoService;
	@Autowired
	AccountService accountService;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    	//システム時刻を取得する
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String nowTimes=df.format(new Date());
    	//期限切れのユーザーIDを取得する
    	List<Integer> userIdList= setInfoService.selectUserId(nowTimes);
    	if(userIdList!=null) {
    		//ユーザーテーブル更新パラメータ
        	Map<String,Object > mapParameter = new HashMap<>();
        	mapParameter.put("memberStatus", "4");
        	mapParameter.put("userId", userIdList);
        	accountService.updateMemberStatus(mapParameter);
    	}


    	//TODO メセッジ送信
    	  return RepeatStatus.FINISHED;
    }
}