package com.example.batch.tasklet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Member;
import com.example.batch.domain.SaveMessage;
import com.example.batch.service.AutocarInsuranceService;
import com.example.batch.service.CarInfoService;
import com.example.batch.service.MemberService;
import com.example.batch.service.SaveMessageService;

@Component
public class AutoCarInsExpiredTasklet implements Tasklet {

	@Autowired
	MemberService memberService;
	@Autowired
	AutocarInsuranceService autocarInsuranceService;
	@Autowired
	CarInfoService carInfoService;
	@Autowired
	SaveMessageService saveMessageService;


    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    	//システム時刻を取得する
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String nowTimes=df.format(new Date());
    	String expiredStatus= "07";
    	Member member;
    	SaveMessage saveMessage =new SaveMessage();

    	//会員番号を取得する
    	List<Integer> carIdlist = autocarInsuranceService.selectExpiredCarId(nowTimes);
    	if(carIdlist!=null&&carIdlist.size()!=0) {
    		List<Integer> memberIdList= carInfoService.selectMemberId(carIdlist);
    		if(memberIdList!=null) {
    			//メールアドレースを取得する
    			//送信する
    			int count=1000;
    			for(Integer memberId: memberIdList) {
    				//メールアドレースを取得する
        			//TODO 送信する
    	        	saveMessage.setSaveMessageId(count++);
    	        	saveMessage.setMemberId(memberId);
    	        	saveMessage.setContent("あと一か月 車検証期限切れメッセージを保存する");
    	        	saveMessageService.insert(saveMessage);

    				member= new Member();
    				member.setMemberId(memberId);
    				//member.setExpiredStatus(expiredStatus);
    				memberService.updateExpiredStatus(member);

    				}
    			}
    		}
    		return RepeatStatus.FINISHED;
    }
}