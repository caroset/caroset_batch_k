package com.example.batch.tasklet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.service.AccountService;
import com.example.batch.service.AutocarInsuranceService;
import com.example.batch.service.CarInfoService;

@Component
public class AutoCarInsuranceTasklet implements Tasklet {

	@Autowired
	AutocarInsuranceService autocarInsuranceService;
	@Autowired
	AccountService accountService;
	@Autowired
	CarInfoService carInfoService;


    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    	//システム時刻を取得する
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String nowTimes=df.format(new Date());
    	//期限切れのユーザーIDを取得する
    	List<Integer> carIdList= autocarInsuranceService.selectCarId(nowTimes);
    	List<Integer> memberIdList;
    	memberIdList=carInfoService.selectMemberId(carIdList);
    	if(memberIdList!=null) {
    		//ユーザーテーブル更新パラメータ
        	Map<String,Object > mapParameter = new HashMap<>();
        	mapParameter.put("memberStatus", "3");
        	mapParameter.put("memberId", memberIdList);
    		accountService.updateMemberStatusList(mapParameter);
    	}

    	//TODO メセッジ送信
    	  return RepeatStatus.FINISHED;
    }
}