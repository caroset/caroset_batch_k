package com.example.batch.tasklet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Account;
import com.example.batch.domain.Member;
import com.example.batch.domain.SaveMessage;
import com.example.batch.service.AccountService;
import com.example.batch.service.MemberService;
import com.example.batch.service.SaveMessageService;

@Component
public class LicenseTermTasklet implements Tasklet {

	@Autowired
	MemberService MemberService;
	@Autowired
	AccountService accountService;
	@Autowired
	SaveMessageService saveMessageService;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    	//システム時刻を取得する
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String nowTimes=df.format(new Date());
    	SaveMessage saveMessage =new SaveMessage();
    	//
    	List<Member> userIdList= MemberService.selectUserId(nowTimes);
    	Account account =new Account();
    	//繰り返す
    	for(Member member: userIdList) {
    		//account.setUserId(member.getUserId());
    		//アカウントの会員ステータスを更新する
    		account.setMemberStatus("1");
    		accountService.updateByPrimaryKeySelective(account);
    		//TODO メセッジ送信
        	saveMessage.setSaveMessageId(1);
        	saveMessage.setMemberId(member.getMemberId());
        	saveMessage.setContent("免許証期限切れメッセージを保存する");
        	saveMessageService.insert(saveMessage);
    	}

    	System.out.println(userIdList.get(0));
    	  return RepeatStatus.FINISHED;
    }
}