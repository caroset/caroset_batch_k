package com.example.batch.processor;

import java.util.List;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.service.CarcassMemberService;
import com.example.batch.service.CarcassMonthlyAmountChargsService;
import com.example.batch.service.MemberAccountService;
import com.example.batch.service.MemberCouponService;
import com.example.batch.service.MemberLicenseService;
import com.example.batch.service.MemberMonthlyAmountChargsService;

public class CmbcProcessor implements ItemProcessor<CarcassCofirmParameter, Map<String,Object>>{

@Autowired
private CarcassMemberService carcassMemberService;

@Autowired
private MemberAccountService  memberAccountService;
@Autowired
private CarcassMonthlyAmountChargsService carcassMonthlyAmountChargsService;

@Autowired
private MemberMonthlyAmountChargsService memberMonthlyAmountChargsService;

@Autowired
private MemberCouponService memberCouponService;

@Autowired
private MemberLicenseService memberLicenseService;



	@Override
	public Map<String, Object> process(CarcassCofirmParameter item) throws Exception {
		int memberId =item.getMemberId();
		//期間内で有効な団体ドメインがない場合は団体会員終了させます
		carcassMemberService.updateCmbc(item.getCarcassMemberId());
		int count =0;
		//会員アカウントのドメインと有効な団体ドメインが一致のデータを取得する
		count = memberAccountService.selectCmbc(memberId);
		//ドメインが一致場合
		if(count!=0) {
			carcassMonthlyAmountChargsService.insertCmbc(item);
		}
		//団体会員レコード有無チェック
		count =carcassMemberService.selectCbmc(item);
		if(count==0) {
			//団体会員の登録
			carcassMemberService.insertCmbc(item);
		}
		//会員月額金の登録
		memberMonthlyAmountChargsService.insertCmbc(item);
		//先月までの会員クーポンを終了されます
		memberCouponService.updateByExpired(memberId);

		//すでに対象団体の会員クーポンがあるか確認します
		count = memberCouponService.selectByMemberId(memberId);
		if(count ==0) {
			memberCouponService.insertCmbc(item);
		}

		//会員クーポン今月有効確認します
		List<Integer> memberCouponIdList =memberCouponService.selectThisMonthValid(memberId);
		//入会クーポンの対象の場合は無料です
		if(memberCouponIdList.size()>=0) {
			for(Integer memberCouponId : memberCouponIdList) {
				item.setMemberCouponId(memberCouponId);
				memberMonthlyAmountChargsService.insertEnrollmentCoupon(item);
			}

		}
		//エリア優待の対象か確認する
		int areaPtViewCouponId = memberLicenseService.selectAreaCheck(memberId);
		if(areaPtViewCouponId!=0) {
			//エリア優待の対象の場合は無料です
			item.setAreaPtViewCouponId(areaPtViewCouponId);
			memberMonthlyAmountChargsService.insertAreaPt(item);
		}
		//エリア優待の会員クーポンがあるか確認します
		count=memberCouponService.selectAreaPt(memberId);
		if(count == 0) {

		}


		//以外の場合
		//TODO条件がなし
		memberMonthlyAmountChargsService.insertOther(item);

		return null;
	}

}
