package com.example.batch.processor;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.batch.domain.MemberRoll;
import com.example.batch.service.MemberRollService;
import com.example.batch.service.RollUnitPriceService;

public class RollProcessor implements ItemProcessor<MemberRoll, Map<String,Object>>{

	@Autowired
	private MemberRoll memberRoll;
	@Autowired
	private MemberRollService memberRollService;
	@Autowired
	private RollUnitPriceService rollUnitPriceService;
	@Override
	public Map<String,Object> process(MemberRoll item) throws Exception {
		//条件実行のフラグ
		String statusFlg = "0";

		//トランザクション処理開始
		//未決済のステータス
		if(item.getMemberRollBillingPaymentStatus().equals("未決済")) {
			statusFlg = "1";
		//決済NGのステータス
		}else if(getMonths(item.getMemberRollBillingPaymentTimes())) {
			statusFlg ="2";
		//上記以外の場合
		}else {
			return null;
		}
		memberRoll.setMemberRollId(item.getMemberRollId());
		memberRoll.setMemberRollBillingPaymentTimes(new Date());
		//前借り単価を取得する
		int rollUnitPrice =rollUnitPriceService.selectRollUnitPrice();
		//未決済⇒決済中変更する
		if(statusFlg.equals("1")) {
			memberRoll.setMemberRollBillingPaymentStatus("決済中");
			memberRoll.setMemberRollBillingAmount(rollUnitPrice*item.getMemberRemainingRollDays());
			//決済NG⇒再決済中を変更する
		}else {
			memberRoll.setMemberRollBillingPaymentStatus("再決済中");
		}
		//会員前借り
		memberRollService.updateByPrimaryKeySelective(memberRoll);
		//TODO GMOにオーソリ連携をする
		//上記によって エラーコードから決済状況を取得する
		//決済完了の場合
		String errCode="";
		if("決済完了の場合".equals(errCode)) {

		}else if("決済完了以外の場合".equals(errCode)) {
		//決済エラー以外の場合
		}else {

		}

		return null;
	}



	public boolean getMonths(Date endTime) {
		//今の時間を取得する（システム時間）
		Date nowTime= new Date();
		Calendar systemDate=Calendar.getInstance();
		Calendar paramDate=Calendar.getInstance();
		systemDate.setTime(nowTime);
		paramDate.setTime(endTime);
		Calendar paramDate10Days=paramDate;
		Calendar paramDate20Days=paramDate;
		Calendar paramDate1Month=paramDate;
		Calendar paramDate2Month=paramDate;
		Calendar paramDate3Month=paramDate;
		paramDate10Days.add(Calendar.DAY_OF_MONTH, 10);
		paramDate20Days.add(Calendar.DAY_OF_MONTH, 20);
		paramDate1Month.add(Calendar.MONTH, 1);
		paramDate2Month.add(Calendar.MONTH, 2);
		paramDate3Month.add(Calendar.MONTH, 3);
		//差が十日の場合
		if(systemDate.compareTo(paramDate10Days)==0) {
			return true;
		}
		//差が二十日の場合
		if(systemDate.compareTo(paramDate20Days)==0) {
			return true;
		}
		//差が一か月の場合
		if(systemDate.compareTo(paramDate1Month)==0) {
			return true;
				}
		//差が二か月の場合
		if(systemDate.compareTo(paramDate2Month)==0) {
			return true;
				}
		//差が三か月の場合
		if(systemDate.compareTo(paramDate3Month)==0) {
			return true;
				}
		return false;

	}
}
