package com.example.batch.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.batch.domain.CsvEntity;
import com.example.batch.domain.MemberMonthlyAmountChargs;
import com.example.batch.service.MemberMonthlyAmountChargsService;
import com.example.batch.util.StringUtil;

public class SettlementProcessor implements ItemProcessor<MemberMonthlyAmountChargs, List<Object>>{

	@Autowired
	private MemberMonthlyAmountChargs memberMonthlyAmountChargs;
	@Autowired
	private MemberMonthlyAmountChargsService memberMonthlyAmountChargsService;
	@Autowired
	private CsvEntity csvEntity;
	@Override
	public List<Object> process(MemberMonthlyAmountChargs item) throws Exception {

		//処理対象を判断する
		Date date = new Date();
		boolean startFlg=false;
		int days= date.getDay();
		int months= date.getMonth();
		int parameterMonths =item.getMmacMonth().getMonth();
		String condition = item.getMmacSettlementCondition();
		memberMonthlyAmountChargs.setMemberMonthlyAmountChargsId(item.getMemberMonthlyAmountChargsId());
		//会員月額課金決済状況
		if("未決済".equals(condition)) {
			startFlg = true;
			memberMonthlyAmountChargs.setMmacSettlementCondition("決済中");
		}else if("決済NG".equals(condition)) {
			//会員月額課金月は当月です
			if(parameterMonths==months&&(days==10||days == 20)){
				startFlg =true;
			}
			//先月以前
			if(months >parameterMonths) {
				startFlg =true;
			}
				memberMonthlyAmountChargs.setMmacSettlementCondition("再決済中");

		//2ヶ月NG
		}else if("2ヶ月NG".equals(condition)) {
			//先々月以前
			if(months -parameterMonths>1) {
				startFlg =true;
			}
			memberMonthlyAmountChargs.setMmacSettlementCondition("再決済中");
		}
		if(!startFlg) {
			return null;
		}
		//会員月額課金決済状況を決済中にする
		memberMonthlyAmountChargsService.updateMmacSettlementCondition(memberMonthlyAmountChargs);
		//ファイル名作成

		SimpleDateFormat dateFormatterYYYYMM = new SimpleDateFormat("YYYYMM");
		//SimpleDateFormat dateFormatterYYMMDD = new SimpleDateFormat("YYMMdd");

		//TODO
		csvEntity.setShopId(0);
		StringBuffer sb1 = new StringBuffer();
		sb1.append(item.getMmacAmount()+"-");
		sb1.append(dateFormatterYYYYMM.format(item.getMmacMonth())+"-");
		sb1.append(StringUtil.leftAddZero(String.valueOf(item.getMemberMonthlyAmountChargsId())));
		csvEntity.setOrderId(sb1.toString());
		//固定値
		csvEntity.setProcessDivision("CAPTURE");
		csvEntity.setUsageAmount(item.getMmacAmount());
		csvEntity.setPaymentMethod("1");
		//TODO決済情報が存在しない
		csvEntity.setMemberId(memberMonthlyAmountChargsService.selectGmoMemberId(item.getMemberId()));
		csvEntity.setRegisterSerialNumberPhysics(memberMonthlyAmountChargsService.selectRSNP(item.getMemberId()));
		csvEntity.setParticipatingStore1(String.valueOf(item.getMmacAmount()));
//		//課金日から三十一日以上経過しているの場合は当月一日
//		long day =(item.getMmacMonth().getTime()-date.getTime())/(24*60*60*1000);
//		if(day>=31) {
//		    Calendar c = Calendar.getInstance();
//	        c.add(Calendar.MONTH, 0);
//	        c.set(Calendar.DAY_OF_MONTH,1);
//			csvEntity.setUseDay(dateFormatterYYMMDD.format(c.getTime()));
//		}else {
//			csvEntity.setUseDay(dateFormatterYYMMDD.format(item.getMmacMonth()));
//		}

		//TODO
//		csvEntity.setDealId();
//		csvEntity.setDealPassword();
//		csvEntity.setTransactionId();
//		csvEntity.setApproval();
//		csvEntity.setDestinationId();
//		csvEntity.setErrCode();
//		csvEntity.setErrDetailsCode();
//		csvEntity.setProcessingDate();
//		//CSV行頭のリスト
//		List<Object> listHeader= new ArrayList<>();
//		listHeader.add("フォーマットバージョン");
//		listHeader.add("ショップID");
//		listHeader.add("オーダーID");
//		listHeader.add("処理区分");
//		listHeader.add("商品コード");
//		listHeader.add("利用金額");
//		listHeader.add("税送料");
//		listHeader.add("支払方法");
//		listHeader.add("支払回数");
//		listHeader.add("カード種別");
//		listHeader.add("会員ID");
//		listHeader.add("登録連番(物理)");
//		listHeader.add("予備");
//		listHeader.add("加盟店自由項目1");
//		listHeader.add("加盟店自由項目2");
//		listHeader.add("加盟店自由項目3");
//		listHeader.add("利用日（YYMMDD）");
//		listHeader.add("取引ID");
//		listHeader.add("取引パスワード");
//		listHeader.add("トランザクションID");
//		listHeader.add("承認番号");
//		listHeader.add("仕向先コード");
//		listHeader.add("エラーコード");
//		listHeader.add("エラー詳細コード");
//		listHeader.add("処理日時");

		//datalist
		List<Object> data = new ArrayList<>();
		data.add(csvEntity.getFormartVersion());
		data.add(csvEntity.getShopId());
		data.add(csvEntity.getOrderId());
		data.add(csvEntity.getProcessDivision());
		data.add(csvEntity.getProductCode());
		data.add(csvEntity.getUsageAmount());
		data.add(csvEntity.getTaxFee());
		data.add(csvEntity.getPaymentMethod());
		data.add(csvEntity.getPaymentTimes());
		data.add(csvEntity.getCardType());
		data.add(csvEntity.getMemberId());
		data.add(csvEntity.getRegisterSerialNumberPhysics());
		data.add(csvEntity.getReserve());
		data.add(csvEntity.getParticipatingStore1());
		data.add(csvEntity.getParticipatingStore2());
		data.add(csvEntity.getParticipatingStore3());
		data.add(csvEntity.getDealId());
		data.add(csvEntity.getDealPassword());
		data.add(csvEntity.getTransactionId());
		data.add(csvEntity.getApproval());
		data.add(csvEntity.getDestinationId());
		data.add(csvEntity.getErrCode());
		data.add(csvEntity.getErrDetailsCode());
		data.add(csvEntity.getProcessingDate());

		return data;
	}

}
