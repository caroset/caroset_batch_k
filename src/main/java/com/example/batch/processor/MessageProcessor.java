package com.example.batch.processor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.batch.domain.Message;
import com.example.batch.domain.NoticeDoList;
import com.example.batch.domain.NoticeQueue;
import com.example.batch.domain.NoticeSet;
import com.example.batch.service.AccountService;
import com.example.batch.service.MessageService;
import com.example.batch.service.NoticeSetService;
import com.example.batch.util.MessageUtil;

public class MessageProcessor implements ItemProcessor<NoticeQueue, Map<String,Object>>{

@Autowired
private NoticeSetService  noticeSetService;
@Autowired
private MessageService messageService;
@Autowired
private AccountService accountService;

	@Override
	public Map<String,Object> process(NoticeQueue item) throws Exception {

		NoticeQueue noticeQueue = item;
		//プッシュとメール送信する区別
		boolean sendPushType =false;
		boolean sendMailType=true;
		Integer memberId = noticeQueue.getMemberId();
		//送信内容とタイトル
		String title;
		String context;
		String mailAddress;
		String token;
		NoticeDoList noticeDoList = new NoticeDoList();
		Map<String,Object> mapParameter =new HashMap<>();

		//パラメータを設定すると 検索をします
		NoticeSet noticeSet = noticeSetService.selectMessageCheck(memberId);
		Map<Integer,Message> messageMap = messageService.selectMessageCheck();
		Message message = messageMap.get(noticeQueue.getMessageId());
		//メッセージ区分＝登録関連
		if("1".equals(message.getMessageClassification())){
			//プッシュ_登録関連が有効の場合
			if("1".equals(noticeSet.getPushRegistRelation())){
				sendPushType = true;
			//メール_登録関連が有効の場合
			}
			if("1".equals(noticeSet.getMailRegistRelation())) {
				sendMailType =true;
			}
		//予約関連の場合
		}else if("2".equals(message.getMessageClassification())) {
			//プッシュ_予約関連が有効の場合
			if("1".equals(noticeSet.getMailReservationRelation())){
				sendPushType=true;
			//メール_予約関連が有効の場合
			}
			if("1".equals(noticeSet.getMailReservationRelation())) {
				sendMailType = true;
			}
		//おすすめの車情報
		}else if("3".equals(message.getMessageClassification())) {
			//プッシュ_おすすめの車情報が有効の場合
			if("1".equals(noticeSet.getPushFeatureCarInfo())){
				sendPushType=true;
			}
		//保存した検索条件の新着
		}else if("4".equals(message.getMessageClassification())) {
			//プッシュ保存した検索条件の新着が有効の場合
			if("1".equals(noticeSet.getPushSaveSearchConditionNewArrivals())){
				sendPushType=true;
			}
		//お気に入りの貸出再開
		}else if("5".equals(message.getMessageClassification())) {
			//お気に入りの貸出再開が有効の場合
			if("1".equals(noticeSet.getPushLikeLoanRestart())){
				sendPushType=true;
			}
		//メッセージ区分がニュース・キャンペーンの場合
		}else if("6".equals(message.getMessageClassification())) {
			//プッシュニュースキャンペーンが有効の場合
			if("1".equals(noticeSet.getPushNewsCampaign())){
				sendPushType=true;
			}
			//メールが満たす
			if("1".equals(noticeSet.getMailNewsCampaign())) {
				sendMailType=true;
			}
		//その他運営からのお知らせ
		}else if("7".equals(message.getMessageClassification())) {
			//プッシュその他運営からのお知らせ
			if("1".equals(noticeSet.getPushOtherOperationFromNotice())) {
				sendPushType=true;
			//メールが満たす
			}
			if("1".equals(noticeSet.getMailOtherOperationFromNotice())) {
				sendMailType=true;
			}
		}

		//プッシュ有効の場合
		if(sendPushType) {
			//送信限定フラグがメール限定の場合
			if("2".equals(message.getSendMailLimitedId())){
				//プッシュ送信無効
				return null;
			}
			title = MessageUtil.messageParametersReplace(message.getPushTitle(),noticeQueue.getMessageParameter() , 0);
			context= MessageUtil.messageParametersReplace(message.getPushMessage(),noticeQueue.getMessageParameter() , 2);
			token = "";
			mapParameter.put("token", token);
			mapParameter.put("pushTitle", title);
			mapParameter.put("pushTcontext", context);

			//上記以外の場合
		}
		if(sendMailType) {
			//送信限定フラグがプッシュ限定の場合
			if("1".equals(message.getSendMailLimitedId())){
				//メール送信無効
				return null;
			}
			title = MessageUtil.messageParametersReplace(message.getMailTitle(),noticeQueue.getMessageParameter() , 0);
			context=MessageUtil.messageParametersReplace(message.getMailMessage(),noticeQueue.getMessageParameter() , 2);
			mailAddress = accountService.selectByPrimaryKey(noticeQueue.getMemberId()).getMail();
			mapParameter.put("mailAddress", mailAddress);
			mapParameter.put("title", title);
			mapParameter.put("context", context);
		}


		//お知らせ・やることリスト
		//やることリストの場合
		if("1".equals(message.getStorageDestination())) {
			noticeDoList.setDoListCount(1);
		//お知らせ
		}else if("2".equals(message.getStorageDestination())) {
			noticeDoList.setNoticeCount(2);
		//ニュース
		}else if("3".equals(message.getStorageDestination())) {
			noticeDoList.setNewsCount(3);
		}
		noticeDoList.setMemberId(memberId);
		mapParameter.put("noticeDoList", noticeDoList);
		mapParameter.put("queueId", noticeQueue.getQueueId());


		return mapParameter;

	}

}
