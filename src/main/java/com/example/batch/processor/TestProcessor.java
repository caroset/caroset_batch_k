package com.example.batch.processor;

import org.springframework.batch.item.ItemProcessor;

import com.example.batch.domain.Account;
import com.example.batch.domain.MemberBak;

public class TestProcessor implements ItemProcessor<Account, MemberBak>{

	@Override
	public MemberBak process(Account item) throws Exception {

		MemberBak member =new MemberBak();
		if(item!=null) {
			member.setMemberId(item.getUserId()+10000);
			member.setUserId(item.getUserId());
			return member;
		}
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

}
