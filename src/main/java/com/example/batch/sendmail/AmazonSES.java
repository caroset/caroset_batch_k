package com.example.batch.sendmail;

import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;

@Configuration
public class AmazonSES {

  // Replace sender@example.com with your "From" address.
  // This address must be verified with Amazon SES.
  static final String FROM = "qiang.zhao@microworld.co.jp";

  // Replace recipient@example.com with a "To" address. If your account
  // is still in the sandbox, this address must be verified.
  private String to;

  // The configuration set to use for this email. If you do not want to use a
  // configuration set, comment the following variable and the
  // .withConfigurationSetName(CONFIGSET); argument below.
  //private String CONFIGSET = "ConfigSet";

  // The subject line for the email.
  private String subject;

  // The HTML body for the email.
  private String htmlbody;

  // The email body for recipients with non-HTML email clients.
  private String textbody;
  //メッセージの内容を初期化する

  @Bean(name="amazonSESField")
  @Scope("prototype")
  public AmazonSES getAmazonSES() {
	  return new AmazonSES();
  }

  public String getTo() {
	return to;
}

public void setTo(String to) {
	this.to = to;
}

public String getSubject() {
	return subject;
}

public void setSubject(String subject) {
	this.subject = subject;
}

public String getHtmlbody() {
	return htmlbody;
}

public void setHtmlbody(String htmlbody) {
	this.htmlbody = htmlbody;
}

public String getTextbody() {
	return textbody;
}

public void setTextbody(String textbody) {
	this.textbody = textbody;
}

public String  sendMail() throws IOException {

	  try {
	      AmazonSimpleEmailService client =
	          AmazonSimpleEmailServiceClientBuilder.standard()
	          // Replace US_WEST_2 with the AWS Region you're using for
	          // Amazon SES.
	            .withRegion(Regions.US_EAST_1).build();
	      SendEmailRequest request = new SendEmailRequest()
	          .withDestination(
	              new Destination().withToAddresses(to))
	          .withMessage(new Message()
	              .withBody(new Body()
	                  .withHtml(new Content()
	                      .withCharset("UTF-8").withData(htmlbody))
	                  .withText(new Content()
	                      .withCharset("UTF-8").withData(textbody)))
	              .withSubject(new Content()
	                  .withCharset("UTF-8").withData(subject)))
	          .withSource(FROM);
	          // Comment or remove the next line if you are not using a
	          // configuration set
	          //.withConfigurationSetName(CONFIGSET);
	      client.sendEmail(request);
	      return "sucess";
	    } catch (Exception ex) {
	    	ex.printStackTrace();
	    	 return "failed";
	    }
  }
}