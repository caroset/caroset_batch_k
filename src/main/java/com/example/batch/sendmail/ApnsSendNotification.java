package com.example.batch.sendmail;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Configuration;

import javapns.devices.Device;
import javapns.devices.implementations.basic.BasicDevice;
import javapns.notification.AppleNotificationServerBasicImpl;
import javapns.notification.PushNotificationManager;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
@Configuration
public class ApnsSendNotification {

	private String deviceToken;

	private String alert;

	private Integer badge;

	private String sound;

	private String certificatePath;

	private String certificatePassword;

//	@Bean(name="apnsSendNotification")
//	public ApnsSendNotification createApnsSendNotification() {
//		return new ApnsSendNotification();
//	}

    public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public Integer getBadge() {
		return badge;
	}

	public void setBadge(Integer badge) {
		this.badge = badge;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

	public String getCertificatePath() {
		return certificatePath;
	}

	public void setCertificatePath(String certificatePath) {
		this.certificatePath = certificatePath;
	}

	public String getCertificatePassword() {
		return certificatePassword;
	}

	public void setCertificatePassword(String certificatePassword) {
		this.certificatePassword = certificatePassword;
	}

	public  Map<String ,Integer>  sendNotification() throws Exception {

        // 设备的 Token 值
        deviceToken = "a4c0bd18e587a05c90775710a431a9dc34183b46940f0f19a0dbb01795801b04";
        // 图标小红圈的数值
        badge = 1;
        // 铃音
        sound = "default";
        Map<String ,Integer> resultMap = new HashMap<>();

        List<String> tokens = new ArrayList<String>();
        tokens.add(deviceToken);

        // 推送证书的路径
        certificatePath = "証明書.p12";
        // 证书的密码
        certificatePassword = "123";// 此处注意导出的证书密码不能为空因为空密码会报错
        boolean sendCount = true;

        try {

            PushNotificationPayload payLoad = new PushNotificationPayload();
            payLoad.addAlert(alert);         // 消息内容
            payLoad.addBadge(badge);         // iphone应用图标上小红圈上的数值、

            if (sound!= null&& "".equals(sound)) {
                payLoad.addSound(sound);    // 铃音
            }
            PushNotificationManager pushManager = new PushNotificationManager();
            // true：表示的是产品发布推送服务 false：表示的是产品测试推送服务
            pushManager
                    .initializeConnection(new AppleNotificationServerBasicImpl(
                            certificatePath, certificatePassword, false));
            List<PushedNotification> notifications = new ArrayList<PushedNotification>();
            // 发送push消息
            if (sendCount) {

                Device device = new BasicDevice();
                device.setToken(tokens.get(0));
                PushedNotification notification = pushManager.sendNotification(
                        device, payLoad, true);
                notifications.add(notification);
            }
            else {

                List<Device> device = new ArrayList<Device>();

                for (String token : tokens) {

                    device.add(new BasicDevice(token));
                }

                notifications = pushManager.sendNotifications(payLoad, device);
            }
            List<PushedNotification> failedNotifications = PushedNotification
                    .findFailedNotifications(notifications);
            List<PushedNotification> successfulNotifications = PushedNotification
                    .findSuccessfulNotifications(notifications);
            //int failed = failedNotifications.size();
            int successful = successfulNotifications.size();
            resultMap.put("successful", successful);
            pushManager.stopConnection();


        }
        catch (Exception e) {

            e.printStackTrace();
        }
        return resultMap;
    }
}

