package com.example.batch.domain;

public class MemberAccount {
    private Integer memberId;

    private String mailAddress;

    private String memberMailDomain;

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress == null ? null : mailAddress.trim();
    }

    public String getMemberMailDomain() {
        return memberMailDomain;
    }

    public void setMemberMailDomain(String memberMailDomain) {
        this.memberMailDomain = memberMailDomain == null ? null : memberMailDomain.trim();
    }
}