package com.example.batch.domain;

import org.springframework.stereotype.Component;

@Component
public class CsvEntity {

	//フォーマとバージョン
	private String formartVersion;

	//ショップID
	private  int shopId;
	//オーダーID
	private String orderId;
	//処理区分
	private  String processDivision;
	//商品コード
	private String productCode;
	//利用金額
	private int UsageAmount;
	//税送料
	private int taxFee;
	//支払方法
	private String paymentMethod;
	//支払回数
	private int paymentTimes;
	//カード種別
	private String cardType;
	//会員ID
	private int memberId;
	//登録連番(物理)
	private int registerSerialNumberPhysics;
	//予備
	private String Reserve;
	//加盟店自由項目1
	private String participatingStore1;
	//加盟店自由項目2
	private String participatingStore2;
	//加盟店自由項目3
	private String participatingStore3;
	//取引ID
	private int dealId;
	//取引パスワード
	private int dealPassword;
	//トランザクションID
	private int transactionId;
	//承認番号
	private int approval;
	//仕向先コード
	private String destinationId;
	//エラーコード
	private String errCode;
	//エラー詳細コード
	private String errDetailsCode;
	//処理日時
	private String processingDate;
	public String getFormartVersion() {
		return formartVersion;
	}
	public void setFormartVersion(String formartVersion) {
		this.formartVersion = formartVersion;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getProcessDivision() {
		return processDivision;
	}
	public void setProcessDivision(String processDivision) {
		this.processDivision = processDivision;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public int getUsageAmount() {
		return UsageAmount;
	}
	public void setUsageAmount(int usageAmount) {
		UsageAmount = usageAmount;
	}
	public int getTaxFee() {
		return taxFee;
	}
	public void setTaxFee(int taxFee) {
		this.taxFee = taxFee;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public int getPaymentTimes() {
		return paymentTimes;
	}
	public void setPaymentTimes(int paymentTimes) {
		this.paymentTimes = paymentTimes;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public int getRegisterSerialNumberPhysics() {
		return registerSerialNumberPhysics;
	}
	public void setRegisterSerialNumberPhysics(int registerSerialNumberPhysics) {
		this.registerSerialNumberPhysics = registerSerialNumberPhysics;
	}
	public String getReserve() {
		return Reserve;
	}
	public void setReserve(String reserve) {
		Reserve = reserve;
	}
	public String getParticipatingStore1() {
		return participatingStore1;
	}
	public void setParticipatingStore1(String participatingStore1) {
		this.participatingStore1 = participatingStore1;
	}
	public String getParticipatingStore2() {
		return participatingStore2;
	}
	public void setParticipatingStore2(String participatingStore2) {
		this.participatingStore2 = participatingStore2;
	}
	public String getParticipatingStore3() {
		return participatingStore3;
	}
	public void setParticipatingStore3(String participatingStore3) {
		this.participatingStore3 = participatingStore3;
	}
//	public String getUseDay() {
//		return useDay;
//	}
//	public void setUseDay(String useDay) {
//		this.useDay = useDay;
//	}
	public int getDealId() {
		return dealId;
	}
	public void setDealId(int dealId) {
		this.dealId = dealId;
	}
	public int getDealPassword() {
		return dealPassword;
	}
	public void setDealPassword(int dealPassword) {
		this.dealPassword = dealPassword;
	}
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	public int getApproval() {
		return approval;
	}
	public void setApproval(int approval) {
		this.approval = approval;
	}
	public String getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getErrDetailsCode() {
		return errDetailsCode;
	}
	public void setErrDetailsCode(String errDetailsCode) {
		this.errDetailsCode = errDetailsCode;
	}
	public String getProcessingDate() {
		return processingDate;
	}
	public void setProcessingDate(String processingDate) {
		this.processingDate = processingDate;
	}
}
