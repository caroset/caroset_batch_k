package com.example.batch.domain;

import java.util.Date;

public class CarInfo {
    private Integer carId;

    private Integer memberId;

    private String maker;

    private Integer carTypeCd;

    private Integer photoId;

    private String smoke;

    private String betto;

    private String distanceSeigen;

    private String place;

    private String lld;

    private String station;

    private String distance;

    private Date deliveryTime;

    private String parkingSize;

    private Double dl;

    private Double ds;

    private Double dls;

    private String catchCopy;

    private String introduction;

    private String insertUser;

    private Date insertTime;

    private String updateUser;

    private Date updateTime;

    private Short version;

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker == null ? null : maker.trim();
    }

    public Integer getCarTypeCd() {
        return carTypeCd;
    }

    public void setCarTypeCd(Integer carTypeCd) {
        this.carTypeCd = carTypeCd;
    }

    public Integer getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }

    public String getSmoke() {
        return smoke;
    }

    public void setSmoke(String smoke) {
        this.smoke = smoke == null ? null : smoke.trim();
    }

    public String getBetto() {
        return betto;
    }

    public void setBetto(String betto) {
        this.betto = betto == null ? null : betto.trim();
    }

    public String getDistanceSeigen() {
        return distanceSeigen;
    }

    public void setDistanceSeigen(String distanceSeigen) {
        this.distanceSeigen = distanceSeigen == null ? null : distanceSeigen.trim();
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place == null ? null : place.trim();
    }

    public String getLld() {
        return lld;
    }

    public void setLld(String lld) {
        this.lld = lld == null ? null : lld.trim();
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station == null ? null : station.trim();
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance == null ? null : distance.trim();
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getParkingSize() {
        return parkingSize;
    }

    public void setParkingSize(String parkingSize) {
        this.parkingSize = parkingSize == null ? null : parkingSize.trim();
    }

    public Double getDl() {
        return dl;
    }

    public void setDl(Double dl) {
        this.dl = dl;
    }

    public Double getDs() {
        return ds;
    }

    public void setDs(Double ds) {
        this.ds = ds;
    }

    public Double getDls() {
        return dls;
    }

    public void setDls(Double dls) {
        this.dls = dls;
    }

    public String getCatchCopy() {
        return catchCopy;
    }

    public void setCatchCopy(String catchCopy) {
        this.catchCopy = catchCopy == null ? null : catchCopy.trim();
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction == null ? null : introduction.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}