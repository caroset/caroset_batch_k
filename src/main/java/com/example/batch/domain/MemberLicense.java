package com.example.batch.domain;

import java.util.Date;

public class MemberLicense {
    private Integer memberId;

    private Integer examinationId;

    private Integer memberLicenseNumber;

    private Date memberLicenseExpirationDate;

    private String memberSurname;

    private String memberName;

    private String memberLicenseAddressPrefectures;

    private String memberLicenseAddressMunicipality;

    private String memberLicenseAddress;

    private String memberLicenseBuildingNameRoomNumber;

    private String memberNowAddressUseFlg;

    private String memberNowAddressPrefectures;

    private String memberNowAddressMunicipality;

    private String memberNowAddress;

    private String memberNowBuildingNameRoomNumber;

    private Integer memberLicenseFrontPhotoId;

    private Integer memberLicenseBackPhotoId;

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getExaminationId() {
        return examinationId;
    }

    public void setExaminationId(Integer examinationId) {
        this.examinationId = examinationId;
    }

    public Integer getMemberLicenseNumber() {
        return memberLicenseNumber;
    }

    public void setMemberLicenseNumber(Integer memberLicenseNumber) {
        this.memberLicenseNumber = memberLicenseNumber;
    }

    public Date getMemberLicenseExpirationDate() {
        return memberLicenseExpirationDate;
    }

    public void setMemberLicenseExpirationDate(Date memberLicenseExpirationDate) {
        this.memberLicenseExpirationDate = memberLicenseExpirationDate;
    }

    public String getMemberSurname() {
        return memberSurname;
    }

    public void setMemberSurname(String memberSurname) {
        this.memberSurname = memberSurname == null ? null : memberSurname.trim();
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName == null ? null : memberName.trim();
    }

    public String getMemberLicenseAddressPrefectures() {
        return memberLicenseAddressPrefectures;
    }

    public void setMemberLicenseAddressPrefectures(String memberLicenseAddressPrefectures) {
        this.memberLicenseAddressPrefectures = memberLicenseAddressPrefectures == null ? null : memberLicenseAddressPrefectures.trim();
    }

    public String getMemberLicenseAddressMunicipality() {
        return memberLicenseAddressMunicipality;
    }

    public void setMemberLicenseAddressMunicipality(String memberLicenseAddressMunicipality) {
        this.memberLicenseAddressMunicipality = memberLicenseAddressMunicipality == null ? null : memberLicenseAddressMunicipality.trim();
    }

    public String getMemberLicenseAddress() {
        return memberLicenseAddress;
    }

    public void setMemberLicenseAddress(String memberLicenseAddress) {
        this.memberLicenseAddress = memberLicenseAddress == null ? null : memberLicenseAddress.trim();
    }

    public String getMemberLicenseBuildingNameRoomNumber() {
        return memberLicenseBuildingNameRoomNumber;
    }

    public void setMemberLicenseBuildingNameRoomNumber(String memberLicenseBuildingNameRoomNumber) {
        this.memberLicenseBuildingNameRoomNumber = memberLicenseBuildingNameRoomNumber == null ? null : memberLicenseBuildingNameRoomNumber.trim();
    }

    public String getMemberNowAddressUseFlg() {
        return memberNowAddressUseFlg;
    }

    public void setMemberNowAddressUseFlg(String memberNowAddressUseFlg) {
        this.memberNowAddressUseFlg = memberNowAddressUseFlg == null ? null : memberNowAddressUseFlg.trim();
    }

    public String getMemberNowAddressPrefectures() {
        return memberNowAddressPrefectures;
    }

    public void setMemberNowAddressPrefectures(String memberNowAddressPrefectures) {
        this.memberNowAddressPrefectures = memberNowAddressPrefectures == null ? null : memberNowAddressPrefectures.trim();
    }

    public String getMemberNowAddressMunicipality() {
        return memberNowAddressMunicipality;
    }

    public void setMemberNowAddressMunicipality(String memberNowAddressMunicipality) {
        this.memberNowAddressMunicipality = memberNowAddressMunicipality == null ? null : memberNowAddressMunicipality.trim();
    }

    public String getMemberNowAddress() {
        return memberNowAddress;
    }

    public void setMemberNowAddress(String memberNowAddress) {
        this.memberNowAddress = memberNowAddress == null ? null : memberNowAddress.trim();
    }

    public String getMemberNowBuildingNameRoomNumber() {
        return memberNowBuildingNameRoomNumber;
    }

    public void setMemberNowBuildingNameRoomNumber(String memberNowBuildingNameRoomNumber) {
        this.memberNowBuildingNameRoomNumber = memberNowBuildingNameRoomNumber == null ? null : memberNowBuildingNameRoomNumber.trim();
    }

    public Integer getMemberLicenseFrontPhotoId() {
        return memberLicenseFrontPhotoId;
    }

    public void setMemberLicenseFrontPhotoId(Integer memberLicenseFrontPhotoId) {
        this.memberLicenseFrontPhotoId = memberLicenseFrontPhotoId;
    }

    public Integer getMemberLicenseBackPhotoId() {
        return memberLicenseBackPhotoId;
    }

    public void setMemberLicenseBackPhotoId(Integer memberLicenseBackPhotoId) {
        this.memberLicenseBackPhotoId = memberLicenseBackPhotoId;
    }
}