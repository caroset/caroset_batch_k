package com.example.batch.domain;

import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class CarcassMember {
    private Integer carcassDomainId;

    private Integer carcassId;

    private Integer memberId;

    private Date carcassMemberJoinDate;

    private Date carcassMemberExclusionDate;

    public Integer getCarcassDomainId() {
        return carcassDomainId;
    }

    public void setCarcassDomainId(Integer carcassDomainId) {
        this.carcassDomainId = carcassDomainId;
    }

    public Integer getCarcassId() {
        return carcassId;
    }

    public void setCarcassId(Integer carcassId) {
        this.carcassId = carcassId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Date getCarcassMemberJoinDate() {
        return carcassMemberJoinDate;
    }

    public void setCarcassMemberJoinDate(Date carcassMemberJoinDate) {
        this.carcassMemberJoinDate = carcassMemberJoinDate;
    }

    public Date getCarcassMemberExclusionDate() {
        return carcassMemberExclusionDate;
    }

    public void setCarcassMemberExclusionDate(Date carcassMemberExclusionDate) {
        this.carcassMemberExclusionDate = carcassMemberExclusionDate;
    }
}