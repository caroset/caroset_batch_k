package com.example.batch.domain;

import java.util.Date;

public class Review {
    private Integer reviewId;

    private String reviewDivision;

    private String reviewStatus;

    private Integer shareId;

    private Integer loandId;

    private Integer reviewerId;

    private String orr;

    private String crr;

    private String aorr;

    private String insertUser;

    private Date insertTime;

    private String updateUser;

    private Date updateTime;

    private Short version;

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public String getReviewDivision() {
        return reviewDivision;
    }

    public void setReviewDivision(String reviewDivision) {
        this.reviewDivision = reviewDivision == null ? null : reviewDivision.trim();
    }

    public String getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(String reviewStatus) {
        this.reviewStatus = reviewStatus == null ? null : reviewStatus.trim();
    }

    public Integer getShareId() {
        return shareId;
    }

    public void setShareId(Integer shareId) {
        this.shareId = shareId;
    }

    public Integer getLoandId() {
        return loandId;
    }

    public void setLoandId(Integer loandId) {
        this.loandId = loandId;
    }

    public Integer getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(Integer reviewerId) {
        this.reviewerId = reviewerId;
    }

    public String getOrr() {
        return orr;
    }

    public void setOrr(String orr) {
        this.orr = orr == null ? null : orr.trim();
    }

    public String getCrr() {
        return crr;
    }

    public void setCrr(String crr) {
        this.crr = crr == null ? null : crr.trim();
    }

    public String getAorr() {
        return aorr;
    }

    public void setAorr(String aorr) {
        this.aorr = aorr == null ? null : aorr.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}