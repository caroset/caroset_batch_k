package com.example.batch.domain;

import java.util.Date;

import org.springframework.context.annotation.Configuration;
@Configuration
public class MemberMonthlyAmountChargs {
    private Integer memberMonthlyAmountChargsId;

    private Integer memberId;

    private Date mmacMonth;

    private Integer mmacAmount;

    private String mmacSettlementCondition;

    private Integer mmacApplyCouponId;

    private Date mmacSettlementDate;

    private String mmacDealId;

    public Integer getMemberMonthlyAmountChargsId() {
        return memberMonthlyAmountChargsId;
    }

    public void setMemberMonthlyAmountChargsId(Integer memberMonthlyAmountChargsId) {
        this.memberMonthlyAmountChargsId = memberMonthlyAmountChargsId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Date getMmacMonth() {
        return mmacMonth;
    }

    public void setMmacMonth(Date mmacMonth) {
        this.mmacMonth = mmacMonth;
    }

    public Integer getMmacAmount() {
        return mmacAmount;
    }

    public void setMmacAmount(Integer mmacAmount) {
        this.mmacAmount = mmacAmount;
    }

    public String getMmacSettlementCondition() {
        return mmacSettlementCondition;
    }

    public void setMmacSettlementCondition(String mmacSettlementCondition) {
        this.mmacSettlementCondition = mmacSettlementCondition == null ? null : mmacSettlementCondition.trim();
    }

    public Integer getMmacApplyCouponId() {
        return mmacApplyCouponId;
    }

    public void setMmacApplyCouponId(Integer mmacApplyCouponId) {
        this.mmacApplyCouponId = mmacApplyCouponId;
    }

    public Date getMmacSettlementDate() {
        return mmacSettlementDate;
    }

    public void setMmacSettlementDate(Date mmacSettlementDate) {
        this.mmacSettlementDate = mmacSettlementDate;
    }

    public String getMmacDealId() {
        return mmacDealId;
    }

    public void setMmacDealId(String mmacDealId) {
        this.mmacDealId = mmacDealId == null ? null : mmacDealId.trim();
    }
}