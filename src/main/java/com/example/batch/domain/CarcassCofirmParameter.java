package com.example.batch.domain;

import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class CarcassCofirmParameter {

	//団体会員ID
	private Integer carcassMemberId;

	public Integer getCarcassMemberId() {
		return carcassMemberId;
	}

	public void setCarcassMemberId(Integer carcassMemberId) {
		this.carcassMemberId = carcassMemberId;
	}

	public Integer getCarcassId() {
		return carcassId;
	}

	public void setCarcassId(Integer carcassId) {
		this.carcassId = carcassId;
	}

	public Date getCarcassContractEndTimes() {
		return carcassContractEndTimes;
	}

	public void setCarcassContractEndTimes(Date carcassContractEndTimes) {
		this.carcassContractEndTimes = carcassContractEndTimes;
	}

	//団体ID
	private Integer carcassId;

	//団体契約終了日
	private Date carcassContractEndTimes;

	//団体ドメイン有効期間終了日
	private Date carcassDomainVpEndDate;

	public Date getCarcassDomainVpEndDate() {
		return carcassDomainVpEndDate;
	}

	public void setCarcassDomainVpEndDate(Date carcassDomainVpEndDate) {
		this.carcassDomainVpEndDate = carcassDomainVpEndDate;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Integer getMemberCouponId() {
		return memberCouponId;
	}

	public void setMemberCouponId(Integer memberCouponId) {
		this.memberCouponId = memberCouponId;
	}

	public Integer getAreaPtViewCouponId() {
		return areaPtViewCouponId;
	}

	public void setAreaPtViewCouponId(Integer areaPtViewCouponId) {
		this.areaPtViewCouponId = areaPtViewCouponId;
	}

	//会員ID
	private Integer memberId;

	//会員クーポンID
	private Integer memberCouponId;

	//エリア優待表示用クーポンID
	private Integer areaPtViewCouponId;



}
