package com.example.batch.domain;

import java.util.Date;

public class CarcassMonthlyAmountChargs {
    private Integer carcassMacId;

    private Integer carcassId;

    private Integer memberId;

    private Date carcassMacDate;

    private Integer carcassMonthlyAmountBasicCharge;

    public Integer getCarcassMacId() {
        return carcassMacId;
    }

    public void setCarcassMacId(Integer carcassMacId) {
        this.carcassMacId = carcassMacId;
    }

    public Integer getCarcassId() {
        return carcassId;
    }

    public void setCarcassId(Integer carcassId) {
        this.carcassId = carcassId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Date getCarcassMacDate() {
        return carcassMacDate;
    }

    public void setCarcassMacDate(Date carcassMacDate) {
        this.carcassMacDate = carcassMacDate;
    }

    public Integer getCarcassMonthlyAmountBasicCharge() {
        return carcassMonthlyAmountBasicCharge;
    }

    public void setCarcassMonthlyAmountBasicCharge(Integer carcassMonthlyAmountBasicCharge) {
        this.carcassMonthlyAmountBasicCharge = carcassMonthlyAmountBasicCharge;
    }
}