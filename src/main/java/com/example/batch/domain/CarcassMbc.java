package com.example.batch.domain;

import java.util.Date;

public class CarcassMbc {
    private Integer carcassMbcId;

    private Integer carcassId;

    private Date carcassMbcVpStartDate;

    private Date carcassMbcVpEndDate;

    private Integer carcassMbc;

    public Integer getCarcassMbcId() {
        return carcassMbcId;
    }

    public void setCarcassMbcId(Integer carcassMbcId) {
        this.carcassMbcId = carcassMbcId;
    }

    public Integer getCarcassId() {
        return carcassId;
    }

    public void setCarcassId(Integer carcassId) {
        this.carcassId = carcassId;
    }

    public Date getCarcassMbcVpStartDate() {
        return carcassMbcVpStartDate;
    }

    public void setCarcassMbcVpStartDate(Date carcassMbcVpStartDate) {
        this.carcassMbcVpStartDate = carcassMbcVpStartDate;
    }

    public Date getCarcassMbcVpEndDate() {
        return carcassMbcVpEndDate;
    }

    public void setCarcassMbcVpEndDate(Date carcassMbcVpEndDate) {
        this.carcassMbcVpEndDate = carcassMbcVpEndDate;
    }

    public Integer getCarcassMbc() {
        return carcassMbc;
    }

    public void setCarcassMbc(Integer carcassMbc) {
        this.carcassMbc = carcassMbc;
    }
}