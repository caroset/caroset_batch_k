package com.example.batch.domain;

import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class MemberCoupon {
    private Integer memberCouponId;

    private Integer memberId;

    private Integer couponId;

    private Date couponGetDate;

    private Date memberCouponExpirationDate;

    public Integer getMemberCouponId() {
        return memberCouponId;
    }

    public void setMemberCouponId(Integer memberCouponId) {
        this.memberCouponId = memberCouponId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Date getCouponGetDate() {
        return couponGetDate;
    }

    public void setCouponGetDate(Date couponGetDate) {
        this.couponGetDate = couponGetDate;
    }

    public Date getMemberCouponExpirationDate() {
        return memberCouponExpirationDate;
    }

    public void setMemberCouponExpirationDate(Date memberCouponExpirationDate) {
        this.memberCouponExpirationDate = memberCouponExpirationDate;
    }
}