package com.example.batch.domain;

import com.example.batch.util.Items;

public class SaveMessage extends Items{
    private Integer saveMessageId;

    private Integer memberId;

    private String content;


    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getMemberId() {
       return this.memberId;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

	public Integer getSaveMessageId() {
		return saveMessageId;
	}

	public void setSaveMessageId(Integer saveMessageId) {
		this.saveMessageId = saveMessageId;
	}
}