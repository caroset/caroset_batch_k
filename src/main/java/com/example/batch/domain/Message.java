package com.example.batch.domain;

import java.util.Date;

public class Message {
    private Integer messageId;

    private Integer sendMailLimitedId;

    private String pushTitle;

    private String mailTitle;

    private String pushMessage;

    private String mailMessage;

    private String messageClassification;

    private String title;

    private String storageDestination;

    private String insertUser;

    private Date insertTime;

    private String updateUser;

    private Date updateTime;

    private Short version;

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public Integer getSendMailLimitedId() {
        return sendMailLimitedId;
    }

    public void setSendMailLimitedId(Integer sendMailLimitedId) {
        this.sendMailLimitedId = sendMailLimitedId;
    }

    public String getPushTitle() {
        return pushTitle;
    }

    public void setPushTitle(String pushTitle) {
        this.pushTitle = pushTitle == null ? null : pushTitle.trim();
    }

    public String getMailTitle() {
        return mailTitle;
    }

    public void setMailTitle(String mailTitle) {
        this.mailTitle = mailTitle == null ? null : mailTitle.trim();
    }

    public String getPushMessage() {
        return pushMessage;
    }

    public void setPushMessage(String pushMessage) {
        this.pushMessage = pushMessage == null ? null : pushMessage.trim();
    }

    public String getMailMessage() {
        return mailMessage;
    }

    public void setMailMessage(String mailMessage) {
        this.mailMessage = mailMessage == null ? null : mailMessage.trim();
    }

    public String getMessageClassification() {
        return messageClassification;
    }

    public void setMessageClassification(String messageClassification) {
        this.messageClassification = messageClassification == null ? null : messageClassification.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getStorageDestination() {
        return storageDestination;
    }

    public void setStorageDestination(String storageDestination) {
        this.storageDestination = storageDestination == null ? null : storageDestination.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}