package com.example.batch.domain;

import java.util.Date;

public class Share {
    private Integer shareId;

    private Integer memberId;

    private Integer state;

    private Integer carId;

    private Date startDate;

    private Date endDate;

    private Date bespeakTime;

    private Integer bespeakDays;

    private Integer useMaybeDays;

    private Integer lastloanDays;

    private String cancelReason;

    private String endDivision;

    private String bespeakMemo;

    private String injuryLoginEndFlg;

    private String myOkFlg;

    private String mycarInjuryOkFlg;

    private String mycarInjuryReturnFlg;

    private String actualFlg;

    private String insertUser;

    private Date insertTime;

    private String updateUser;

    private Date updateTime;

    private Short version;

    public Integer getShareId() {
        return shareId;
    }

    public void setShareId(Integer shareId) {
        this.shareId = shareId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getBespeakTime() {
        return bespeakTime;
    }

    public void setBespeakTime(Date bespeakTime) {
        this.bespeakTime = bespeakTime;
    }

    public Integer getBespeakDays() {
        return bespeakDays;
    }

    public void setBespeakDays(Integer bespeakDays) {
        this.bespeakDays = bespeakDays;
    }

    public Integer getUseMaybeDays() {
        return useMaybeDays;
    }

    public void setUseMaybeDays(Integer useMaybeDays) {
        this.useMaybeDays = useMaybeDays;
    }

    public Integer getLastloanDays() {
        return lastloanDays;
    }

    public void setLastloanDays(Integer lastloanDays) {
        this.lastloanDays = lastloanDays;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason == null ? null : cancelReason.trim();
    }

    public String getEndDivision() {
        return endDivision;
    }

    public void setEndDivision(String endDivision) {
        this.endDivision = endDivision == null ? null : endDivision.trim();
    }

    public String getBespeakMemo() {
        return bespeakMemo;
    }

    public void setBespeakMemo(String bespeakMemo) {
        this.bespeakMemo = bespeakMemo == null ? null : bespeakMemo.trim();
    }

    public String getInjuryLoginEndFlg() {
        return injuryLoginEndFlg;
    }

    public void setInjuryLoginEndFlg(String injuryLoginEndFlg) {
        this.injuryLoginEndFlg = injuryLoginEndFlg == null ? null : injuryLoginEndFlg.trim();
    }

    public String getMyOkFlg() {
        return myOkFlg;
    }

    public void setMyOkFlg(String myOkFlg) {
        this.myOkFlg = myOkFlg == null ? null : myOkFlg.trim();
    }

    public String getMycarInjuryOkFlg() {
        return mycarInjuryOkFlg;
    }

    public void setMycarInjuryOkFlg(String mycarInjuryOkFlg) {
        this.mycarInjuryOkFlg = mycarInjuryOkFlg == null ? null : mycarInjuryOkFlg.trim();
    }

    public String getMycarInjuryReturnFlg() {
        return mycarInjuryReturnFlg;
    }

    public void setMycarInjuryReturnFlg(String mycarInjuryReturnFlg) {
        this.mycarInjuryReturnFlg = mycarInjuryReturnFlg == null ? null : mycarInjuryReturnFlg.trim();
    }

    public String getActualFlg() {
        return actualFlg;
    }

    public void setActualFlg(String actualFlg) {
        this.actualFlg = actualFlg == null ? null : actualFlg.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}