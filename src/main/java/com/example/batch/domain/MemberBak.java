package com.example.batch.domain;

import java.util.Date;

import com.example.batch.util.Items;

public class MemberBak  extends Items{
	//key
    private Integer memberId;

    private Integer userId;

    private String expiredStatus;

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


	//key
    private String name;

    private Date birthday;

    private String licenseCity;

    private String licenseArea;

    private String licenseStreet;

    private String licenseBuilding;

    private String nowAddrFlg;

    private String nowAddrCity;

    private String nowAddrArea;

    private String nowAddrStreet;

    private String nowAddrBuilding;

    private String sex;

    private Integer telNumber;

    private Integer ninsyouCd;

    private Date ninsyouTerm;

    private Date licenseTerm;

    private Integer licenseNumber;

    private String memberType;

    private Integer invitationCd;

    private String fixedSearchCon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getLicenseCity() {
        return licenseCity;
    }

    public void setLicenseCity(String licenseCity) {
        this.licenseCity = licenseCity == null ? null : licenseCity.trim();
    }

    public String getLicenseArea() {
        return licenseArea;
    }

    public void setLicenseArea(String licenseArea) {
        this.licenseArea = licenseArea == null ? null : licenseArea.trim();
    }

    public String getLicenseStreet() {
        return licenseStreet;
    }

    public void setLicenseStreet(String licenseStreet) {
        this.licenseStreet = licenseStreet == null ? null : licenseStreet.trim();
    }

    public String getLicenseBuilding() {
        return licenseBuilding;
    }

    public void setLicenseBuilding(String licenseBuilding) {
        this.licenseBuilding = licenseBuilding == null ? null : licenseBuilding.trim();
    }

    public String getNowAddrFlg() {
        return nowAddrFlg;
    }

    public void setNowAddrFlg(String nowAddrFlg) {
        this.nowAddrFlg = nowAddrFlg == null ? null : nowAddrFlg.trim();
    }

    public String getNowAddrCity() {
        return nowAddrCity;
    }

    public void setNowAddrCity(String nowAddrCity) {
        this.nowAddrCity = nowAddrCity == null ? null : nowAddrCity.trim();
    }

    public String getNowAddrArea() {
        return nowAddrArea;
    }

    public void setNowAddrArea(String nowAddrArea) {
        this.nowAddrArea = nowAddrArea == null ? null : nowAddrArea.trim();
    }

    public String getNowAddrStreet() {
        return nowAddrStreet;
    }

    public void setNowAddrStreet(String nowAddrStreet) {
        this.nowAddrStreet = nowAddrStreet == null ? null : nowAddrStreet.trim();
    }

    public String getNowAddrBuilding() {
        return nowAddrBuilding;
    }

    public void setNowAddrBuilding(String nowAddrBuilding) {
        this.nowAddrBuilding = nowAddrBuilding == null ? null : nowAddrBuilding.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public Integer getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(Integer telNumber) {
        this.telNumber = telNumber;
    }

    public Integer getNinsyouCd() {
        return ninsyouCd;
    }

    public void setNinsyouCd(Integer ninsyouCd) {
        this.ninsyouCd = ninsyouCd;
    }

    public Date getNinsyouTerm() {
        return ninsyouTerm;
    }

    public void setNinsyouTerm(Date ninsyouTerm) {
        this.ninsyouTerm = ninsyouTerm;
    }

    public Date getLicenseTerm() {
        return licenseTerm;
    }

    public void setLicenseTerm(Date licenseTerm) {
        this.licenseTerm = licenseTerm;
    }

    public Integer getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(Integer licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType == null ? null : memberType.trim();
    }

    public Integer getInvitationCd() {
        return invitationCd;
    }

    public void setInvitationCd(Integer invitationCd) {
        this.invitationCd = invitationCd;
    }

    public String getFixedSearchCon() {
        return fixedSearchCon;
    }

    public void setFixedSearchCon(String fixedSearchCon) {
        this.fixedSearchCon = fixedSearchCon == null ? null : fixedSearchCon.trim();
    }


	public String getExpiredStatus() {
		return expiredStatus;
	}

	public void setExpiredStatus(String expiredStatus) {
		this.expiredStatus = expiredStatus;
	}
}