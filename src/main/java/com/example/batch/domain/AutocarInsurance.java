package com.example.batch.domain;

import java.util.Date;

public class AutocarInsurance {
    private Integer carId;

    private Integer insuranceCardId;

    private String insuranceCardPhoto;

    private Integer insuranceKaisyaId;

    private Date insuranceStartDate;

    private Date insuranceEndDate;

    private String contractorName;

    private Integer insuranceNumber;

    private String contractorNameRelation;

    private String photo;

    private String insertUser;

    private Date insertTime;

    private String updateUser;

    private Date updateTime;

    private Short version;

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getInsuranceCardId() {
        return insuranceCardId;
    }

    public void setInsuranceCardId(Integer insuranceCardId) {
        this.insuranceCardId = insuranceCardId;
    }

    public String getInsuranceCardPhoto() {
        return insuranceCardPhoto;
    }

    public void setInsuranceCardPhoto(String insuranceCardPhoto) {
        this.insuranceCardPhoto = insuranceCardPhoto == null ? null : insuranceCardPhoto.trim();
    }

    public Integer getInsuranceKaisyaId() {
        return insuranceKaisyaId;
    }

    public void setInsuranceKaisyaId(Integer insuranceKaisyaId) {
        this.insuranceKaisyaId = insuranceKaisyaId;
    }

    public Date getInsuranceStartDate() {
        return insuranceStartDate;
    }

    public void setInsuranceStartDate(Date insuranceStartDate) {
        this.insuranceStartDate = insuranceStartDate;
    }

    public Date getInsuranceEndDate() {
        return insuranceEndDate;
    }

    public void setInsuranceEndDate(Date insuranceEndDate) {
        this.insuranceEndDate = insuranceEndDate;
    }

    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName == null ? null : contractorName.trim();
    }

    public Integer getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(Integer insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getContractorNameRelation() {
        return contractorNameRelation;
    }

    public void setContractorNameRelation(String contractorNameRelation) {
        this.contractorNameRelation = contractorNameRelation == null ? null : contractorNameRelation.trim();
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}