package com.example.batch.domain;

import java.util.Date;

public class NoticeSet {
    private Integer terminalId;

    private Integer memberId;

    private String pushRegistRelation;

    private String pushReservationRelation;

    private String pushFeatureCarInfo;

    private String pushSaveSearchConditionNewArrivals;

    private String pushLikeLoanRestart;

    private String pushNewsCampaign;

    private String pushOtherOperationFromNotice;

    private String mailRegistRelation;

    private String mailReservationRelation;

    private String mailNewsCampaign;

    private String mailOtherOperationFromNotice;

    private String insertUser;

    private Date insertTime;

    private String updateUser;

    private Date updateTime;

    private Short version;

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getPushRegistRelation() {
        return pushRegistRelation;
    }

    public void setPushRegistRelation(String pushRegistRelation) {
        this.pushRegistRelation = pushRegistRelation == null ? null : pushRegistRelation.trim();
    }

    public String getPushReservationRelation() {
        return pushReservationRelation;
    }

    public void setPushReservationRelation(String pushReservationRelation) {
        this.pushReservationRelation = pushReservationRelation == null ? null : pushReservationRelation.trim();
    }

    public String getPushFeatureCarInfo() {
        return pushFeatureCarInfo;
    }

    public void setPushFeatureCarInfo(String pushFeatureCarInfo) {
        this.pushFeatureCarInfo = pushFeatureCarInfo == null ? null : pushFeatureCarInfo.trim();
    }

    public String getPushSaveSearchConditionNewArrivals() {
        return pushSaveSearchConditionNewArrivals;
    }

    public void setPushSaveSearchConditionNewArrivals(String pushSaveSearchConditionNewArrivals) {
        this.pushSaveSearchConditionNewArrivals = pushSaveSearchConditionNewArrivals == null ? null : pushSaveSearchConditionNewArrivals.trim();
    }

    public String getPushLikeLoanRestart() {
        return pushLikeLoanRestart;
    }

    public void setPushLikeLoanRestart(String pushLikeLoanRestart) {
        this.pushLikeLoanRestart = pushLikeLoanRestart == null ? null : pushLikeLoanRestart.trim();
    }

    public String getPushNewsCampaign() {
        return pushNewsCampaign;
    }

    public void setPushNewsCampaign(String pushNewsCampaign) {
        this.pushNewsCampaign = pushNewsCampaign == null ? null : pushNewsCampaign.trim();
    }

    public String getPushOtherOperationFromNotice() {
        return pushOtherOperationFromNotice;
    }

    public void setPushOtherOperationFromNotice(String pushOtherOperationFromNotice) {
        this.pushOtherOperationFromNotice = pushOtherOperationFromNotice == null ? null : pushOtherOperationFromNotice.trim();
    }

    public String getMailRegistRelation() {
        return mailRegistRelation;
    }

    public void setMailRegistRelation(String mailRegistRelation) {
        this.mailRegistRelation = mailRegistRelation == null ? null : mailRegistRelation.trim();
    }

    public String getMailReservationRelation() {
        return mailReservationRelation;
    }

    public void setMailReservationRelation(String mailReservationRelation) {
        this.mailReservationRelation = mailReservationRelation == null ? null : mailReservationRelation.trim();
    }

    public String getMailNewsCampaign() {
        return mailNewsCampaign;
    }

    public void setMailNewsCampaign(String mailNewsCampaign) {
        this.mailNewsCampaign = mailNewsCampaign == null ? null : mailNewsCampaign.trim();
    }

    public String getMailOtherOperationFromNotice() {
        return mailOtherOperationFromNotice;
    }

    public void setMailOtherOperationFromNotice(String mailOtherOperationFromNotice) {
        this.mailOtherOperationFromNotice = mailOtherOperationFromNotice == null ? null : mailOtherOperationFromNotice.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}