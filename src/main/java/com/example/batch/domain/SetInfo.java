package com.example.batch.domain;

import java.util.Date;

public class SetInfo {
    private Integer setId;

    private Integer memberId;

    private Integer userId;

    private String cardLastYonketa;

    private Date effTerm;

    private String tokun;

    private Integer gmoMemberId;

    private Integer registerSerialNumberPhysics;

    public Integer getSetId() {
        return setId;
    }

    public void setSetId(Integer setId) {
        this.setId = setId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCardLastYonketa() {
        return cardLastYonketa;
    }

    public void setCardLastYonketa(String cardLastYonketa) {
        this.cardLastYonketa = cardLastYonketa == null ? null : cardLastYonketa.trim();
    }

    public Date getEffTerm() {
        return effTerm;
    }

    public void setEffTerm(Date effTerm) {
        this.effTerm = effTerm;
    }

    public String getTokun() {
        return tokun;
    }

    public void setTokun(String tokun) {
        this.tokun = tokun == null ? null : tokun.trim();
    }

    public Integer getGmoMemberId() {
        return gmoMemberId;
    }

    public void setGmoMemberId(Integer gmoMemberId) {
        this.gmoMemberId = gmoMemberId;
    }

    public Integer getRegisterSerialNumberPhysics() {
        return registerSerialNumberPhysics;
    }

    public void setRegisterSerialNumberPhysics(Integer registerSerialNumberPhysics) {
        this.registerSerialNumberPhysics = registerSerialNumberPhysics;
    }
}