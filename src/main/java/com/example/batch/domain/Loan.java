package com.example.batch.domain;

import java.util.Date;

public class Loan {
	//key

    private Integer loanId;

    private Integer shareId;

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Integer getShareId() {
        return shareId;
    }

    public void setShareId(Integer shareId) {
        this.shareId = shareId;
    }

	//key
    private Integer memberId;

    private Date requestReceptTime;

    private Date endDate;

    private Date stateDivision;

    private String cancelReason;

    private String loanMemo;

    private String actualFlg;

    private String insertUser;

    private Date insertTime;

    private String updateUser;

    private Short version;

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Date getRequestReceptTime() {
        return requestReceptTime;
    }

    public void setRequestReceptTime(Date requestReceptTime) {
        this.requestReceptTime = requestReceptTime;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStateDivision() {
        return stateDivision;
    }

    public void setStateDivision(Date stateDivision) {
        this.stateDivision = stateDivision;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason == null ? null : cancelReason.trim();
    }

    public String getLoanMemo() {
        return loanMemo;
    }

    public void setLoanMemo(String loanMemo) {
        this.loanMemo = loanMemo == null ? null : loanMemo.trim();
    }

    public String getActualFlg() {
        return actualFlg;
    }

    public void setActualFlg(String actualFlg) {
        this.actualFlg = actualFlg == null ? null : actualFlg.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}