package com.example.batch.domain;

import java.util.Date;

public class RollUnitPrice {
    private Integer rollUnitPriceId;

    private Date rollUnitPriceVpStartDate;

    private Date rollUnitPriceVpEndDate;

    private Integer rollUnitPrice;

    public Integer getRollUnitPriceId() {
        return rollUnitPriceId;
    }

    public void setRollUnitPriceId(Integer rollUnitPriceId) {
        this.rollUnitPriceId = rollUnitPriceId;
    }

    public Date getRollUnitPriceVpStartDate() {
        return rollUnitPriceVpStartDate;
    }

    public void setRollUnitPriceVpStartDate(Date rollUnitPriceVpStartDate) {
        this.rollUnitPriceVpStartDate = rollUnitPriceVpStartDate;
    }

    public Date getRollUnitPriceVpEndDate() {
        return rollUnitPriceVpEndDate;
    }

    public void setRollUnitPriceVpEndDate(Date rollUnitPriceVpEndDate) {
        this.rollUnitPriceVpEndDate = rollUnitPriceVpEndDate;
    }

    public Integer getRollUnitPrice() {
        return rollUnitPrice;
    }

    public void setRollUnitPrice(Integer rollUnitPrice) {
        this.rollUnitPrice = rollUnitPrice;
    }
}