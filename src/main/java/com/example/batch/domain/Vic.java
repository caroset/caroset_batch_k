package com.example.batch.domain;

import java.util.Date;

public class Vic {
    private Integer carId;

    private Integer vicId;

    private Integer vehicleNumber;

    private String vicPhoto;

    private Date fyey;

    private Date dateOfValidity;

    private Integer rideCapacity;

    private String ownerName;

    private String insertUser;

    private Date insertTime;

    private String updateUser;

    private Date updateTime;

    private Short version;

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getVicId() {
        return vicId;
    }

    public void setVicId(Integer vicId) {
        this.vicId = vicId;
    }

    public Integer getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(Integer vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVicPhoto() {
        return vicPhoto;
    }

    public void setVicPhoto(String vicPhoto) {
        this.vicPhoto = vicPhoto == null ? null : vicPhoto.trim();
    }

    public Date getFyey() {
        return fyey;
    }

    public void setFyey(Date fyey) {
        this.fyey = fyey;
    }

    public Date getDateOfValidity() {
        return dateOfValidity;
    }

    public void setDateOfValidity(Date dateOfValidity) {
        this.dateOfValidity = dateOfValidity;
    }

    public Integer getRideCapacity() {
        return rideCapacity;
    }

    public void setRideCapacity(Integer rideCapacity) {
        this.rideCapacity = rideCapacity;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName == null ? null : ownerName.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}