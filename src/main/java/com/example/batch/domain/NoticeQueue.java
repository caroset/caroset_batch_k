package com.example.batch.domain;

import java.util.Date;

public class NoticeQueue {
    private Integer queueId;

    private Integer terminalId;

    private Integer memberId;

    private Integer messageId;

    private String messageParameter;

    private String messageSendFlg;

    private String insertUser;

    private Date insertTime;

    private String updateUser;

    private Date updateTime;

    private Short version;

    public Integer getQueueId() {
        return queueId;
    }

    public void setQueueId(Integer queueId) {
        this.queueId = queueId;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public String getMessageParameter() {
        return messageParameter;
    }

    public void setMessageParameter(String messageParameter) {
        this.messageParameter = messageParameter == null ? null : messageParameter.trim();
    }

    public String getMessageSendFlg() {
        return messageSendFlg;
    }

    public void setMessageSendFlg(String messageSendFlg) {
        this.messageSendFlg = messageSendFlg == null ? null : messageSendFlg.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}