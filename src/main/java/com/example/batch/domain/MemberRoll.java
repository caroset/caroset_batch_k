package com.example.batch.domain;

import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class MemberRoll {
    private Integer memberRollId;

    private Integer shareId;

    private Integer memberId;

    private Date memberRollOccurrenceTimes;

    private Date memberRollTermTimes;

    private Integer memberRollDays;

    private Integer memberRemainingRollDays;

    private Integer memberRollBillingAmount;

    private Date memberRollBillingPaymentTimes;

    private String memberRollBillingPaymentStatus;

    private Integer memberRollBillingPaymentDealId;

    public Integer getMemberRollId() {
        return memberRollId;
    }

    public void setMemberRollId(Integer memberRollId) {
        this.memberRollId = memberRollId;
    }

    public Integer getShareId() {
        return shareId;
    }

    public void setShareId(Integer shareId) {
        this.shareId = shareId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Date getMemberRollOccurrenceTimes() {
        return memberRollOccurrenceTimes;
    }

    public void setMemberRollOccurrenceTimes(Date memberRollOccurrenceTimes) {
        this.memberRollOccurrenceTimes = memberRollOccurrenceTimes;
    }

    public Date getMemberRollTermTimes() {
        return memberRollTermTimes;
    }

    public void setMemberRollTermTimes(Date memberRollTermTimes) {
        this.memberRollTermTimes = memberRollTermTimes;
    }

    public Integer getMemberRollDays() {
        return memberRollDays;
    }

    public void setMemberRollDays(Integer memberRollDays) {
        this.memberRollDays = memberRollDays;
    }

    public Integer getMemberRemainingRollDays() {
        return memberRemainingRollDays;
    }

    public void setMemberRemainingRollDays(Integer memberRemainingRollDays) {
        this.memberRemainingRollDays = memberRemainingRollDays;
    }

    public Integer getMemberRollBillingAmount() {
        return memberRollBillingAmount;
    }

    public void setMemberRollBillingAmount(Integer memberRollBillingAmount) {
        this.memberRollBillingAmount = memberRollBillingAmount;
    }

    public Date getMemberRollBillingPaymentTimes() {
        return memberRollBillingPaymentTimes;
    }

    public void setMemberRollBillingPaymentTimes(Date memberRollBillingPaymentTimes) {
        this.memberRollBillingPaymentTimes = memberRollBillingPaymentTimes;
    }

    public String getMemberRollBillingPaymentStatus() {
        return memberRollBillingPaymentStatus;
    }

    public void setMemberRollBillingPaymentStatus(String memberRollBillingPaymentStatus) {
        this.memberRollBillingPaymentStatus = memberRollBillingPaymentStatus == null ? null : memberRollBillingPaymentStatus.trim();
    }

    public Integer getMemberRollBillingPaymentDealId() {
        return memberRollBillingPaymentDealId;
    }

    public void setMemberRollBillingPaymentDealId(Integer memberRollBillingPaymentDealId) {
        this.memberRollBillingPaymentDealId = memberRollBillingPaymentDealId;
    }
}