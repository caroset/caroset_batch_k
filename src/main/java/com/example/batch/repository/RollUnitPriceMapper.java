package com.example.batch.repository;

import com.example.batch.domain.RollUnitPrice;

public interface RollUnitPriceMapper {
    int deleteByPrimaryKey(Integer rollUnitPriceId);

    int insert(RollUnitPrice record);

    int insertSelective(RollUnitPrice record);

    RollUnitPrice selectByPrimaryKey(Integer rollUnitPriceId);

    int updateByPrimaryKeySelective(RollUnitPrice record);

    int updateByPrimaryKey(RollUnitPrice record);

    int selectRollUnitPrice();
}