package com.example.batch.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.Share;

public interface ShareMapper {
    int deleteByPrimaryKey(Integer shareId);

    int insert(Share record);

    int insertSelective(Share record);

    Share selectByPrimaryKey(Integer shareId);

    int updateByPrimaryKeySelective(Share record);

    int updateByPrimaryKey(Share record);
    List<Integer> selectBy30MinutePush(@Param("times") String times);

    List<Integer> selectMemberId(@Param("stateKubun") String stateKubun, @Param("times") String times);
}