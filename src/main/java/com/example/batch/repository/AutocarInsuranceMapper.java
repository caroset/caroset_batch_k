package com.example.batch.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.AutocarInsurance;

public interface AutocarInsuranceMapper {
    int deleteByPrimaryKey(Integer carId);

    int insert(AutocarInsurance record);

    int insertSelective(AutocarInsurance record);

    AutocarInsurance selectByPrimaryKey(Integer carId);

    int updateByPrimaryKeySelective(AutocarInsurance record);

    int updateByPrimaryKey(AutocarInsurance record);

    List<Integer> selectCarId(@Param("times")String times);

    List<Integer> selectExpiredCarId(@Param("times") String times);
}