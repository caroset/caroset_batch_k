package com.example.batch.repository;

import com.example.batch.domain.SaveMessage;

public interface SaveMessageMapper {
    int deleteByPrimaryKey(Integer messageId);

    int insert(SaveMessage record);

    int insertSelective(SaveMessage record);

    SaveMessage selectByPrimaryKey(Integer messageId);

    int updateByPrimaryKeySelective(SaveMessage record);

    int updateByPrimaryKey(SaveMessage record);
}