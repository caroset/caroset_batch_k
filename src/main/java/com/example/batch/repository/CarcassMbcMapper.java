package com.example.batch.repository;

import com.example.batch.domain.CarcassMbc;

public interface CarcassMbcMapper {
    int deleteByPrimaryKey(Integer carcassMbcId);

    int insert(CarcassMbc record);

    int insertSelective(CarcassMbc record);

    CarcassMbc selectByPrimaryKey(Integer carcassMbcId);

    int updateByPrimaryKeySelective(CarcassMbc record);

    int updateByPrimaryKey(CarcassMbc record);
}