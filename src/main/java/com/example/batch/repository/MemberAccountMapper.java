package com.example.batch.repository;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.MemberAccount;

public interface MemberAccountMapper {
    int deleteByPrimaryKey(Integer memberId);

    int insert(MemberAccount record);

    int insertSelective(MemberAccount record);

    MemberAccount selectByPrimaryKey(Integer memberId);

    int updateByPrimaryKeySelective(MemberAccount record);

    int updateByPrimaryKey(MemberAccount record);
    Integer selectCmbc(@Param("memberId") Integer memberId);
}