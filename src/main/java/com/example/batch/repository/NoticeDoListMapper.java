package com.example.batch.repository;

import com.example.batch.domain.NoticeDoList;

public interface NoticeDoListMapper {
    int deleteByPrimaryKey(Integer terminalId);

    int insert(NoticeDoList record);

    int insertSelective(NoticeDoList record);

    NoticeDoList selectByPrimaryKey(Integer terminalId);

    int updateByPrimaryKeySelective(NoticeDoList record);

    int updateByPrimaryKey(NoticeDoList record);

    void updateByMemberId(NoticeDoList noticeDoList);
}