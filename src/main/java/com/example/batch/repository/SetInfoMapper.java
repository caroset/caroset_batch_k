package com.example.batch.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.SetInfo;

public interface SetInfoMapper {
    int deleteByPrimaryKey(Integer setId);

    int insert(SetInfo record);

    int insertSelective(SetInfo record);

    SetInfo selectByPrimaryKey(Integer setId);

    int updateByPrimaryKeySelective(SetInfo record);

    int updateByPrimaryKey(SetInfo record);

    SetInfo selectByPrimaryKey(SetInfo setInfo);
    List<Integer> selectUserId(@Param("times") String times );

    List<Integer> selectExpiredMemberId(@Param("times") String times);
}