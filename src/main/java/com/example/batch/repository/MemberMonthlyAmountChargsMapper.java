package com.example.batch.repository;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.domain.MemberMonthlyAmountChargs;

public interface MemberMonthlyAmountChargsMapper {
    int deleteByPrimaryKey(Integer memberMonthlyAmountChargsId);

    int insert(MemberMonthlyAmountChargs record);

    int insertSelective(MemberMonthlyAmountChargs record);

    MemberMonthlyAmountChargs selectByPrimaryKey(Integer memberMonthlyAmountChargsId);

    int updateByPrimaryKeySelective(MemberMonthlyAmountChargs record);

    int updateByPrimaryKey(MemberMonthlyAmountChargs record);

    int insertCmbc(CarcassCofirmParameter carcassCofirmParameter);

    int insertEnrollmentCoupon(CarcassCofirmParameter carcassCofirmParameter);

    int insertAreaPt(CarcassCofirmParameter carcassCofirmParameter);

    int insertOther(CarcassCofirmParameter carcassCofirmParameter);

    MemberMonthlyAmountChargs selectSettlement();

    int updateMmacSettlementCondition(MemberMonthlyAmountChargs memberMonthlyAmountChargs);

    int selectGmoMemberId(@Param("memberId") Integer memberId);
    int selectRSNP(@Param("memberId") Integer memberId);

}