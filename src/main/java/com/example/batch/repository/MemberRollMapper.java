package com.example.batch.repository;

import com.example.batch.domain.MemberRoll;

public interface MemberRollMapper {
    int deleteByPrimaryKey(Integer memberRollId);

    int insert(MemberRoll record);

    int insertSelective(MemberRoll record);

    MemberRoll selectByPrimaryKey(Integer memberRollId);

    int updateByPrimaryKeySelective(MemberRoll record);

    int updateByPrimaryKey(MemberRoll record);
}