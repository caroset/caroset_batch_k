package com.example.batch.repository;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.NoticeSet;

public interface NoticeSetMapper {
    int deleteByPrimaryKey(Integer terminalId);

    int insert(NoticeSet record);

    int insertSelective(NoticeSet record);

    NoticeSet selectByPrimaryKey(Integer terminalId);

    int updateByPrimaryKeySelective(NoticeSet record);

    int updateByPrimaryKey(NoticeSet record);

    NoticeSet selectMessageCheck(@Param("memberId")Integer memberId);
}