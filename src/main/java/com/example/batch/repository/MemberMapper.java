package com.example.batch.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.Member;

public interface MemberMapper {
    int deleteByPrimaryKey(Member key);

    int insert(Member record);

    int insertSelective(Member record);

    Member selectByPrimary(Member key);

    int updateByPrimaryKeySelective(Member record);

    int updateByPrimaryKey(Member record);

    List<Member> selectUserId(@Param("times")String times);
    Member selectByVicId(Member member);

    List<Integer> selectByExpiredStatus(@Param("expiredStatus") String expiredStatus,@Param("times") String times);

    int updateExpiredStatus(Member member);

    int selectByMemberId(@Param("memberId") Integer selectByMemberId);

    Member selectCmbc();
}