package com.example.batch.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.Vic;

public interface VicMapper {
    int deleteByPrimaryKey(Integer carId);

    int insert(Vic record);

    int insertSelective(Vic record);

    Vic selectByPrimaryKey(Integer carId);

    int updateByPrimaryKeySelective(Vic record);

    int updateByPrimaryKey(Vic record);

    List<Vic> selectVicId(@Param("times")String times);
    List<Integer> selectCarId(@Param("times") String times);
}