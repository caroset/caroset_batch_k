package com.example.batch.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.domain.MemberCoupon;

public interface MemberCouponMapper {
    int deleteByPrimaryKey(Integer memberCouponId);

    int insert(MemberCoupon record);

    int insertSelective(MemberCoupon record);

    MemberCoupon selectByPrimaryKey(Integer memberCouponId);

    int updateByPrimaryKeySelective(MemberCoupon record);

    int updateByPrimaryKey(MemberCoupon record);
    int updateCmbc(@Param("memberId") Integer memberId);

    int insertCmbc(CarcassCofirmParameter memberCoupon);

    int updateByMemberId(@Param("memberId") Integer memberId);

    int updateByExpired(@Param("memberId") Integer memberId);

    int updateByAreaPt(@Param("memberId") Integer memberId);

    int selectByMemberId(@Param("memberId")Integer memberId);

    List<Integer> selectThisMonthValid(@Param("memberId")Integer memberId);
    int selectAreaPt(@Param("memberId")Integer memberId);
    int updateUseEndDay(@Param("memberId")Integer memberId);

}