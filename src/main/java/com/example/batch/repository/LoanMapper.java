package com.example.batch.repository;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.Loan;

public interface LoanMapper {
    int deleteByPrimaryKey(Loan key);

    int insert(Loan record);

    int insertSelective(Loan record);

    Loan selectByPrimaryKey(Loan key);

    int updateByPrimaryKeySelective(Loan record);

    int updateByPrimaryKey(Loan record);
    List<Integer> selectMemberId(@Param("stateKubun") String stateKubun,@Param("times") String times);
}