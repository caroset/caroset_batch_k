package com.example.batch.repository;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.domain.CarcassMonthlyAmountChargs;

public interface CarcassMonthlyAmountChargsMapper {
    int deleteByPrimaryKey(Integer carcassMacId);

    int insert(CarcassMonthlyAmountChargs record);

    int insertSelective(CarcassMonthlyAmountChargs record);

    CarcassMonthlyAmountChargs selectByPrimaryKey(Integer carcassMacId);

    int updateByPrimaryKeySelective(CarcassMonthlyAmountChargs record);

    int updateByPrimaryKey(CarcassMonthlyAmountChargs record);

    int insertCmbc(CarcassCofirmParameter carcassCofirmParameter);
}