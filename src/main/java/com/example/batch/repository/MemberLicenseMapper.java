package com.example.batch.repository;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.MemberLicense;

public interface MemberLicenseMapper {
    int deleteByPrimaryKey(Integer memberId);

    int insert(MemberLicense record);

    int insertSelective(MemberLicense record);

    MemberLicense selectByPrimaryKey(Integer memberId);

    int updateByPrimaryKeySelective(MemberLicense record);

    int updateByPrimaryKey(MemberLicense record);

    int selectAreaCheck(@Param("memberId")Integer memberId);
}