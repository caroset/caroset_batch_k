package com.example.batch.repository;

import com.example.batch.domain.NoticeQueue;

public interface NoticeQueueMapper {
    int deleteByPrimaryKey(Integer queueId);

    int insert(NoticeQueue record);

    int insertSelective(NoticeQueue record);

    NoticeQueue selectByPrimaryKey(Integer queueId);

    int updateByPrimaryKeySelective(NoticeQueue record);

    int updateByPrimaryKey(NoticeQueue record);

    NoticeQueue selectAll();
}