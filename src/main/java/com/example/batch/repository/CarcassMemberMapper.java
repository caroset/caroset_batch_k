package com.example.batch.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.example.batch.domain.CarcassCofirmParameter;
import com.example.batch.domain.CarcassMember;

public interface CarcassMemberMapper {
    int deleteByPrimaryKey(Integer carcassDomainId);

    int insert(CarcassMember record);

    int insertSelective(CarcassMember record);

    CarcassMember selectByPrimaryKey(Integer carcassDomainId);

    int updateByPrimaryKeySelective(CarcassMember record);

    int updateByPrimaryKey(CarcassMember record);

    List<CarcassMember> selectCmbc(@Param("memberId") Integer memberId);

    int updateCmbc(@Param("carcassMemberId") Integer carcassMemberId);

    int insertCmbc(CarcassMember carcassMember);

    CarcassCofirmParameter selectByMemberId(@Param("memberId") Integer memberId);

    int selectCbmc(CarcassCofirmParameter carcassCofirmParameter);

   int insertCmbc(CarcassCofirmParameter carcassCofirmParameter);
}