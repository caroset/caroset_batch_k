package com.example.batch.repository;

import java.util.List;
import java.util.Map;

import com.example.batch.domain.Account;

/**
 * @ClassName: AccountMapper
 * @Description: mybites数据查询接口
 */
public interface AccountMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);
    int updateMemberStatus(Map<String, Object> map);

    int updateMemberStatusList(Map<String ,Object> map);

    List<Account> selectAll();
}